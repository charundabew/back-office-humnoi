export interface ISubMenu {
  id: number;
  name: string;
  is_active: boolean;
  path: string;
  key: string;
}
export interface IMenuItem {
  menu: string;
  key: string;
  path: string;
  submenu: ISubMenu[] | null;
  element?: any;
  icon: any;
}
