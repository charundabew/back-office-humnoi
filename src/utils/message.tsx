import { message, notification } from "antd";

export const messageAction = (
  action: "saveSuccess" | "saveFail" | "deleteSuccess" | "deleteFail" | 'getError',
  error?: string
) => {
  switch (action) {
    case "saveSuccess":
      return message.success(`บันทึกข้อมูลสำเร็จ`);
    case "saveFail":
      return notification.error({
        message: 'ไม่สามารถบันทึกข้อมูลได้',
        description: `Error ${error}`
      });
    case "deleteSuccess":
      return message.success(`ลบข้อมูลสำเร็จ`);
    case "deleteFail":
      return message.error(`ไม่สามารถลบข้อมูลได้ กรุณาลองใหม่อีกครั้ง`); 
    case "getError":
      return notification.error({
        message: 'เกิดข้อผิดพลาด',
        description: `Error ${error}`
      });
    default:
      break;
  }
};
