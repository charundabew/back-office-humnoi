import React from "react";
import { Switch } from "antd";

const ColumnsModalPermission: any[] = [
  {
    title: "ชื่อเมนูการเข้าใช้งาน",
    dataIndex: "menu_name",
    key: "cat_name",
    width: 200,
  },
  {
    title: "ดูรายละเอียด",
    key: "view",
    align: "center",
    render: (row: any) => <Switch defaultChecked={row.view} />,
  },
  {
    title: "เพิ่มข้อมูล",
    key: "add",
    align: "center",
    render: (row: any) => <Switch defaultChecked={row.add} />,
  },
  {
    title: "แก้ไขข้อมูล",
    key: "edit",
    align: "center",
    render: (row: any) => <Switch defaultChecked={row.edit} />,
  },
  {
    title: "ลบข้อมูล",
    key: "delete",
    align: "center",
    render: (row: any) => <Switch defaultChecked={row.delete} />,
  },
];

export default ColumnsModalPermission;
