
const orderStatus = [
  {
    value: 1,
    label: "รอชำระ",
    created_at: "2022-06-17 14:05:42",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 2,
    label: "รอตรวจสอบ",
    created_at: "2022-06-17 14:05:42",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 3,
    label: "ชำระเงินสำเร็จ",
    created_at: "2022-06-17 14:05:41",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 4,
    label: "เตรียมจัดส่ง",
    created_at: "2022-06-17 14:05:41",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 5,
    label: "ที่ต้องได้รับ",
    created_at: "2022-06-17 14:05:41",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 6,
    label: "ส่งสำเร็จ",
    created_at: "2022-06-17 14:05:40",
    updated_at: null,
    deleted_at: null,
  },
  {
    value: 7,
    label: "ชำระเงินไม่สำเร็จ",
    created_at: null,
    updated_at: null,
    deleted_at: null,
  },
];

export default orderStatus

