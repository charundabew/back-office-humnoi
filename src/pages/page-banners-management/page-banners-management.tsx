import {
  Avatar,
  Carousel,
  Typography,
  message,
  Popconfirm,
  Spin,
  Row,
  Col,
  Skeleton,
} from "antd";
import Dragger from "antd/lib/upload/Dragger";
import React, { useState, useEffect } from "react";
import SortableList, { SortableItem } from "react-easy-sort";
import IconAddImage from "src/assets/icons/add-image.png";
import { BoxStyled, ButtonCustom, HeaderPage } from "src/components";
import styled from "styled-components";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { IBanners } from "src/interfaces/interface-response";
import { renderImage } from "src/utils/helper";
import { RcFile } from "antd/lib/upload";
import { messageAction } from "src/utils/message";
import { LoadingOutlined } from "@ant-design/icons";
import _ from "lodash";

const { Title, Text } = Typography;
export default function PageBannersManagement() {
  const [loading, setLoading] = useState(false);
  const [banners, setBanners] = useState<IBanners[]>([]);
  const [idIsDeleted, setIdIsDeleted] = useState<number[]>([]);
  const [uploadImage, setUploadImage] = useState(false);
  const onSortEnd = (oldIndex: number, newIndex: number) => {
    const updateIndex = banners;
    updateIndex[oldIndex] = { ...updateIndex[oldIndex], index: newIndex };
    const sortImage = _.sortBy(updateIndex, "index");
    setBanners([...sortImage]);
  };
  const onFetchData = () => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.BANNERS,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const sortImage = _.sortBy(data.data, "index");
        setBanners(sortImage);
        setLoading(false);
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });
  };
  useEffect(onFetchData, []);

  const onConfirmDelete = (idToDelete: number) => {
    const removedItem = banners.filter(
      (item: IBanners) => item.id !== idToDelete
    );
    setBanners(removedItem);
    setIdIsDeleted([...idIsDeleted, idToDelete]);
  };

  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type.match("image.*");
    if (!isJpgOrPng) {
      message.error("กรุณาอัปโหลดเฉพาะไฟล์รูปภาพเท่านั้น");
    } else {
      onUploadImage(file);
    }
  };
  const onUploadImage = (file: RcFile) => {
    setUploadImage(true);
    const formData = new FormData();
    formData.append("banners_img", file);
    AXIOS({
      method: "post",
      url: API.BANNERS,
      data: formData,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        onFetchData();
        messageAction("saveSuccess");
        setUploadImage(false);
      })
      .catch((error: AxiosError<any, any>) => {
        setUploadImage(false);
        messageAction("saveFail", error.message);
      });
  };
  const onSubmit = () => {
    setLoading(true);
    const formData = {
      banner_img: banners,
      delete_imgs: idIsDeleted,
    };
    AXIOS({
      method: "put",
      url: API.BANNERS,
      data: formData,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        onFetchData();
        messageAction("saveSuccess");
        setLoading(false);
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
        messageAction("saveFail", error.message);
      });
  };
  return (
    <div>
      <HeaderPage
        header="ตั้งค่าแบนเนอร์ "
        subHeader="สามารถเพิ่ม/ลบ/แก้ไข แบนเนอร์ได้"
      />
      <div className="App-paper-page">
        <Spin spinning={loading} tip="กำลังโหลดข้อมูล...">
          <BoxStyled color="white">
            <Title level={5}>ตัวอย่างการแสดงผลแบนเนอร์</Title>
            <div style={{ marginTop: "1rem" }}>
              <Carousel>
                {loading ? (
                  <div>
                    <Skeleton active paragraph={{ rows: 6 }} />
                  </div>
                ) : (
                  banners.map((item: IBanners) => (
                    <div key={item.id}>
                      <img
                        alt="slide"
                        src={renderImage(item.img_path)}
                        style={imagetStyle}
                      ></img>
                    </div>
                  ))
                )}
              </Carousel>
            </div>
            <div style={{ marginTop: "2rem", marginBottom: "1rem" }}>
              <Text strong style={{ fontSize: "16px" }}>
                จัดการตำแหน่งรูปภาพ
              </Text>
              <Text style={{ marginLeft: "1rem" }}>
                ** สามารถลากจัดตำแหน่งของรูปภาพในการแสดงผลได้
              </Text>
            </div>
            {loading ? (
              <div>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
            ) : (
              <SortItemList
                onSortEnd={onSortEnd}
                draggedItemClassName="dragged"
                style={{ gridTemplateColumns: "unset" }}
              >
                <Row gutter={[16, 16]}>
                  {banners.map((banner: IBanners, index: number) => (
                    <SortableItem key={banner.id}>
                      <Col xl={8}>
                        <img
                          style={{ borderRadius: "10px" }}
                          width={"100%"}
                          height="150px"
                          alt="sortImage"
                          src={renderImage(banner.img_path)}
                        />
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        >
                          <Text strong>{`รูปภาพที่ : ${index + 1}`}</Text>
                          <Popconfirm
                            title="ต้องการลบรูปภาพนี้?"
                            onConfirm={() => onConfirmDelete(banner.id)}
                            okText="ตกลง"
                            cancelText="ยกเลิก"
                          >
                            <ButtonCustom type="text" text="ลบรูปภาพ" />
                          </Popconfirm>
                        </div>
                      </Col>
                    </SortableItem>
                  ))}
                  <Col xl={8}>
                    <DraggerStyle
                      beforeUpload={beforeUpload}
                      showUploadList={false}
                      multiple={false}
                    >
                      {uploadImage ? (
                        <div>
                          <LoadingOutlined style={{ fontSize: 48 }} />
                          <DivText>
                            <TextHightLight>กำลังอัปโหลดรูปภาพ</TextHightLight>
                          </DivText>
                          <DivTextSub>
                            <TextGrey type="secondary">
                              กรุณารอสักครู่...
                            </TextGrey>
                          </DivTextSub>
                        </div>
                      ) : (
                        <div>
                          <Avatar
                            shape="square"
                            size={48}
                            src={IconAddImage}
                          ></Avatar>
                          <DivText>
                            <TextHightLight>อัปโหลดไฟล์</TextHightLight>
                            <TextGrey type="secondary">
                              หรือ ลากและวางรูปภาพ
                            </TextGrey>
                          </DivText>
                          <DivTextSub>
                            <TextGrey type="secondary">
                              PNG, JPG, GIF up tp 10MB
                            </TextGrey>
                          </DivTextSub>
                        </div>
                      )}
                    </DraggerStyle>
                  </Col>
                </Row>
              </SortItemList>
            )}
          </BoxStyled>
        </Spin>

        <Row justify="end" style={{ marginTop: "1rem" }}>
          <Col xl={3}>
            <ButtonCustom
              size="large"
              width="100%"
              icon="save"
              type="primary"
              text="บันทึกข้อมูล"
              onClick={onSubmit}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
}

const DivTextSub = styled.div`
  color: #90959e;
  margin-top: 0.1rem;
`;
const DivText = styled.div`
  margin-top: 1rem;
`;
const TextHightLight = styled.span`
  color: #5151f9;
  font-weight: 500;
  font-size: 14px;
`;
const TextGrey = styled(Text)`
  font-size: 12px;
  margin-left: 10px;
`;
const imagetStyle: React.CSSProperties = {
  height: "40vh",
  width: "100%",
  borderRadius: 5,
};
const SortItemList = styled(SortableList)`
  user-select: none;
  display: grid;
  grid-template-columns: auto auto auto;
  grid-gap: 16px;
`;

const DraggerStyle = styled(Dragger)`
  &.ant-upload.ant-upload-drag {
    border: 1px dashed #90959e;
    border-radius: 10px;
    background: white;
    height: 153px;
  }
`;
