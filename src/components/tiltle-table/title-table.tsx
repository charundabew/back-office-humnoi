import React from "react";
import { Typography } from "antd";
import styled from "styled-components";
const { Text} = Typography;

interface IPropsTitle {
  title: string;
  children?: React.ReactNode;
}
const DivStyle = styled.div`
  margin: 1.2rem 2rem 1rem 2rem;
`;
export default function TitleTable({ title, children }: IPropsTitle) {
  return (
    <DivStyle>
      <Text style={{fontWeight: 600,fontSize: '14px'}}>{title}</Text>
      {children}
    </DivStyle>
  );
}
