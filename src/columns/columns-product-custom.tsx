import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { Avatar, Tag } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const ColumnsProductXustom: ColumnsType<any> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 10,
    // fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "รูปสินค้า",
    dataIndex: "product_image",
    key: "product_image",
    align: "center",
    width: 30,
    render: (_, item) => (
      <Avatar shape="square" size={48} src={item.product_image} />
    ),
  },
  {
    title: "สีสินค้า",
    dataIndex: "product_color",
    key: "product_color",
    width: 100,
  },
  {
    title: "ราคา/ชิ้น",
    dataIndex: "price",
    key: "price",
    align: "center",
    width: 40,
  },
  {
    title: "สินค้าในคลัง",
    dataIndex: "stock",
    key: "stock",
    align: "center",
    width: 50,
  },

  {
    title: "แก้ไข",
    key: "action",
    width: 20,
    align: "center",
    render: () => <Tag color='blue' icon={<EditOutlined />}>แก้ไขสินค้า</Tag>
  },
  {
    title: "ลบ",
    key: "action",
    width: 20,
    align: "center",
    render: () => <Tag  color='error' icon={<DeleteOutlined />}>ลบสินค้า</Tag>
  },
];

export default ColumnsProductXustom;
