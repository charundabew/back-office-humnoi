import React from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";

export const propsModalDelete = {
  title: "ต้องการลบรายการนี้?",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsModalSave = {
  title: "ต้องการบันทึกข้อมูล?",
  icon: <ExclamationCircleOutlined />,
  content:
    "คลิกยินยันหากระบุข้อมูลถูกต้อง ทั้งนี้สามารถแก้ไขรายละเอียดภายหลังได้",
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsModalApprovePayment = {
  title: "ยืนยันการโอนเงินผ่านธนาคาร?",
  content:
    "คลิกยินยันหากตรวจสอบยอดชำระเงินถูกต้องแล้ว  (หากยืนยันแล้ว จะไม่สามารถแก้ไขข้อมูลได้)",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsModalDeniedPayment = {
  title: "ยืนยัน “ยกเลิก” การโอนเงินผ่านธนาคาร?",
  content:
    "คลิกยินยันหากต้องการให้ผู้สั่งซื้อ แจ้งรายละเอียดการโอนเงินอีกครั้ง",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsConfirmShipment = {
  title: "ยืนยันการส่งสินค้า?",
  content:
    "คลิกยินยันหากระบุข้อมูลการจัดส่งครบถ้วนแล้ว (หากยืนยันแล้ว จะไม่สามารถแก้ไขข้อมูลได้)",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsConfirmAddProduct = {
  title: "ยืนยันการบันทึกข้อมูลสินค้า?",
  content:
    "คลิกยินยันหากระบุข้อมูลสินค้าครบถ้วนแล้ว ทั้งนี้สามารถแก้ไขรายละเอียดภายหลังได้",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
export const propsConfirmLogout = {
  title: "ต้องการออกจากระบบ?",
  content: "คลิกยินยันหากต้องการลงชื่อออกจากระบบ",
  icon: <ExclamationCircleOutlined />,
  okText: "ตกลง",
  cancelText: "ยกเลิก",
};
