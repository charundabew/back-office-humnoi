import React, { useState, useEffect, useContext } from "react";
import {
  BoxStyled,
  TitleForm,
  FormUploadImage,
  HeaderSubPage,
  ButtonCustom,
  FormUploadCoverImage,
} from "src/components";
import {
  Modal,
  Col,
  Form,
  Input,
  notification,
  Radio,
  Row,
  Select,
  Spin,
  TreeSelect,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import styled from "styled-components";
import {
  USE_HOOK_BRAND,
  USE_HOOK_PRODUCT_ID,
  USE_HOOK_SUB_CATEGORY,
} from "src/hooks";
import {
  IFormCoverImage,
  IFormImageProduct,
  IFormProductListSingle,
  IFormProductSingle,
  IFormProductStep1,
  IOptionsStep1,
} from "src/interfaces/interface-form";
import _ from "lodash";
import {
  IBrands,
  ICategories,
  IResStepFirst,
} from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import axios, { AxiosError, AxiosResponse } from "axios";
import { messageAction } from "src/utils/message";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { useNavigate, useParams } from "react-router-dom";
import { formProductSingle } from "src/data/data-initial-value";
import { convertToPrice } from "src/utils/pattern-number";
import { propsConfirmAddProduct } from "src/utils/props-component";
import { onKeyDownOnlyNumber } from "src/utils/helper";
import { RcFile } from "antd/lib/upload";
const { Option } = Select;
const { confirm } = Modal;
interface ILoading {
  loading: boolean;
  tip: "กำลังโหลดข้อมูล..." | "กำลังบันทึกข้อมูล..." | "";
}
export default function PageSingleProduct() {
  const [form] = Form.useForm();
  const navigator = useNavigate();
  const { productId, parentId, name } = useParams();
  const { uploadedImage, onSetUploaded, coverImage, onSetToStore } =
    useContext(GlobalContext);
  const [idDelete, setIdDelete] = useState<number[]>([]);
  const [totalValue, setTotalValue] = useState("");
  const [loading, setLoading] = useState<ILoading>({
    loading: false,
    tip: "",
  });
  const [options, setOptions] = useState<IOptionsStep1>({
    optionsBrand: [],
    optionsCatergory: [],
  });

  const createBranch: any = ({ sub_categories, name, id }: ICategories) => {
    if (sub_categories && sub_categories.length > 0) {
      return {
        title: name,
        value: id,
        children: sub_categories.map((sub: ICategories) => {
          return createBranch(sub);
        }),
      };
    } else {
      return {
        title: name,
        value: id,
        children: [],
      };
    }
  };
  useEffect(() => {
    // declare the data fetching function
    const onPrepareForm = async () => {
      setLoading({
        loading: true,
        tip: "กำลังโหลดข้อมูล...",
      });
      const responseBrands = await USE_HOOK_BRAND();
      const responseCategory = await USE_HOOK_SUB_CATEGORY(parentId ?? "");
      setOptions({
        optionsBrand: responseBrands,
        optionsCatergory: responseCategory.map((item: ICategories) =>
          createBranch(item)
        ),
      });
      if (productId === undefined) {
        form.setFieldsValue({
          ...formProductSingle,
        });
        onSetUploaded([]);
        onResetImage()
        setLoading({
          loading: false,
          tip: "",
        });
       
      } else {
        const resFormData: IFormProductSingle = await USE_HOOK_PRODUCT_ID(
          productId
        );
        const firstItem: IFormProductListSingle =
          resFormData.product_lists[0] ?? formProductSingle;
        onSetToStore({
          name: "coverImage",
          value: {
            imagePath: resFormData.image_path,
            file: {} as RcFile,
            base64: "",
            product_list_id: firstItem.id,
          } as IFormCoverImage,
        });
        form.setFieldsValue({
          ...resFormData,
        });
        const updateItem = resFormData?.product_lists?.map(
          (item: IFormProductListSingle) => {
            return {
              image: item.image_file,
              product_id: item.product_id,
              color: item.color,
              stock: item.stock,
              base64: "",
              uid: "",
              id: item.id,
            };
          }
        );
        onUpdateTotal();
        onSetUploaded(updateItem ?? []);
        setLoading({
          loading: false,
          tip: "",
        });
      }
    };

    // call the function
    onPrepareForm();
    // make sure to catch any error
  }, [productId]);
const onResetImage = () => {
  onSetToStore({
    name: "coverImage",
    value: {
      imagePath:'' ,
      file: {} as RcFile,
      base64: "",
      product_list_id: NaN,
    } as IFormCoverImage,
  });
}
  const onPostImageFile = (response: IResStepFirst) => {
    const endpoints = uploadedImage.map((item: IFormImageProduct) => {
      if (!item.id) {
        const newProduct = {
          image: item.image,
          product_id: response.product_id,
          color: item.color,
          stock: Number(item.stock),
        };
        return newProduct;
      } else {
        const oldProduct = {
          product_list_id: item.id,
          product_id: response.product_id,
          color: item.color,
          stock: Number(item.stock),
        };
        return oldProduct;
      }
    });
    axios
      .all(
        endpoints.map((endpoint: IFormImageProduct) => {
          let formData = new FormData();
          _.forIn(endpoint, (value: any, key: string) => {
            formData.append(key, value);
          });
          return AXIOS({
            method: "POST",
            url: API.STEP_FORM_SECONDE,
            data: formData,
          });
        })
      )
      .then(() => {
        if (idDelete.length > 0) {
          onDeleteItemList();
        } else {
          onResetImage()
          messageAction("saveSuccess");
          navigator(-1);
          setLoading({
            loading: false,
            tip: "",
          });
        }
      })
      .catch(() => {
        setLoading({
          loading: false,
          tip: "",
        });
        Modal.error({
          title: "พบรายการสินค้าที่มีปัญหา",
          content: `กรุณาตรวจสอบข้อมูลสินค้าบางรายการ`,
          onOk: () => {
            if (idDelete.length > 0) {
              onDeleteItemList();
            } else {
              messageAction("saveSuccess");
              navigator(-1);
              setLoading({
                loading: false,
                tip: "",
              });
            }
          },
        });
      });
  };
  const onDeleteItemList = () => {
    AXIOS({
      method: "post",
      url: API.PRODUCT_LIST_ITEM,
      data: { product_list_id: idDelete },
    })
      .then(() => {
        messageAction("saveSuccess");
        navigator(-1);
        onResetImage()
        setLoading({
          loading: false,
          tip: "",
        });
      })
      .catch(() => {
        messageAction("deleteFail");
      });
  };
  const onFinish = (values: IFormProductStep1) => {
    if (
      uploadedImage.some(
        (item: IFormImageProduct) => !item.color || !item.stock
      )
    ) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description:
          "กรุณาตรวจสอบชื่อสีหรือจำนวนสินค้าใน stock ของรูปภาพสินค้าที่อัปโหลด",
      });
    } else if (!coverImage.imagePath && !coverImage.base64) {
      return notification.warning({
        message: "กรุณาอัปโหลดรูปภาพหน้าปก",
        description: "กรุณาตรวจสอบรูปภาพหน้าปกก่อนการบันทึกรายการ",
      });
    } else {
      confirm({
        ...propsConfirmAddProduct,
        onOk() {
          return new Promise((resolve: any, reject: any) => {
            setLoading({
              loading: true,
              tip: "กำลังบันทึกข้อมูล...",
            });
            const { price, qty, discount } = values;
            const data = {
              ...values,
              id: productId === undefined ? null : Number(productId),
              price: Number(price),
              qty: Number(qty),
              discount: Number(discount),
            };
            const formData = new FormData();
            if (coverImage.base64) {
              formData.append("cover_image", coverImage.file);
              formData.append("obj", JSON.stringify(data));
            } else {
              formData.append("obj", JSON.stringify(data));
            }
            AXIOS({
              method: productId === undefined ? "post" : "put",
              url: API.STEP_FORM_FIRST,
              params: { product_id: productId ?? null },
              data: formData,
            })
              .then(({ data }: AxiosResponse<any, any>) => {
                onPostImageFile(data.data);
                resolve();
              })
              .catch((error: AxiosError<any, any>) => {
                reject();
                messageAction("saveFail", error.message);
                setLoading({
                  loading: false,
                  tip: "",
                });
              });
          });
        },
      });
    }
  };

  const onUpdateTotal = () => {
    const values = form.getFieldsValue();
    const { price, discount } = values;
    let result = Number(price ?? 0) - Number(discount ?? 0);
    setTotalValue(convertToPrice(result));
  };
  const onClickCancle = () => {
    form.resetFields();
    navigator(-1);
  };
  const validateMessages = {
    required: "ระบุข้อมูล ${label}",
  };
  const onUpdateDelete = (itemId: number) => {
    setIdDelete([...idDelete, itemId]);
  };

  return (
    <div>
      <HeaderSubPage
        header={productId ? `จัดการรายละเอียด` : `เพิ่มสินค้าประเภท${name}`}
        subHeader={
          productId
            ? `จัดการรายละเอียดสินค้า ${name}`
            : `ตรวจสอบข้อมูลสินค้าให้ครบถ้วนก่อนการบันทึกผล`
        }
      />
      <div className="App-paper-page">
        <Spin spinning={loading.loading} tip={loading.tip}>
          <Form
            scrollToFirstError
            form={form}
            layout="vertical"
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <TitleForm type="form" />
            <BoxStyled color="white">
              <Row gutter={[4, 4]} justify="center">
                <Col xl={12} sm={6} xs={24}>
                  <Form.Item
                    name="name"
                    label="ชื่อสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input size="large" placeholder="ระบุชื่อสินค้า" />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={6} xs={24}>
                  <Form.Item
                    name="brand_id"
                    required
                    label="แบรนด์สินค้า"
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="เลือกแบรนด์สินค้า"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        (option!.children as unknown as string)
                          .toLowerCase()
                          .includes(input.toLowerCase())
                      }
                    >
                      {options.optionsBrand.map((brand: IBrands) => (
                        <Option key={brand.id} value={brand.id}>
                          {brand.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item name="category_id" required label="ประเภทสินค้า">
                    <TreeSelect
                      showSearch
                      style={{ width: "100%" }}
                      dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                      placeholder="เลือกประเภทสินค้า"
                      allowClear
                      treeDefaultExpandAll
                      treeData={options.optionsCatergory}
                    ></TreeSelect>
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item
                    name="qty"
                    required
                    label="จำนวนสินค้าต่อหน่วย"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      size="large"
                      placeholder="0 ชิ้น"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item
                    name="price"
                    required
                    label="ราคาสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item name="discount" label="ส่วนลด">
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item label="ราคารวม">
                    <Input
                      disabled
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                      value={totalValue}
                    />
                  </Form.Item>
                </Col>
                <Col xl={24}>
                  <Form.Item name="detail" label="รายละเอียดสินค้า">
                    <TextArea style={{ minHeight: "5rem" }} />
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="free_shipping" required label="การจัดส่ง">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>มีค่าบริการจัดส่ง</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>จัดส่งฟรี</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="is_active" required label="สถานะสินค้า ">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>เปิดใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>ระงับการใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
            </BoxStyled>
            <TitleForm type="images" />
            <BoxStyled color="white">
              <Row gutter={[8, 8]}>
                <Col xl={8} sm={12} xs={24}>
                  <FormUploadCoverImage title="รูปภาพหน้าปก" />
                </Col>
              </Row>
              <div style={{ marginTop: "20px" }}>
                <FormUploadImage onUpdateDelete={onUpdateDelete} />
              </div>
            </BoxStyled>
            <div style={{ marginTop: "1rem" }}>
              <Row justify="space-between">
                <Col xl={3}>
                  <ButtonCustom
                    width="100%"
                    icon="cancel"
                    onClick={onClickCancle}
                    type="ghost"
                    text="ยกเลิกข้อมูล"
                  />
                </Col>
                <Col xl={3}>
                  <Form.Item>
                    <ButtonCustom
                      htmlType="submit"
                      width="100%"
                      icon="save"
                      type="primary"
                      text="บันทึกข้อมูล"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </div>
          </Form>
        </Spin>
      </div>
    </div>
  );
}

const DivCheckBox = styled.div`
  padding: 12px 20px;
  border-radius: 5px;
  background: #f6f8fa;
`;
const SpanCheckbox = styled.span`
  color: #46505e;
`;
