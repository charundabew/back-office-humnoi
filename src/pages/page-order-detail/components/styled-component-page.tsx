import styled from "styled-components";
import { Typography } from "antd";
const { Text } = Typography;

export const DivRoot = styled.div`
  background: white;
  padding: 18px;
  border-radius: 10px;
  height: 100%;
  position:relative;

`;

export const DivDarkGrey = styled.div`
  bottom: 10px;
  background: #ececec;
  padding: 1rem;
  border-radius: 5px;
`;
export const DivBorder = styled.div`
  background: white;
  padding: 1rem;
  border-radius: 5px;
  border: 1px solid #ececec;
`;
export const Text14 = styled(Text)`
  font-size: 14px;
`;
export const TextParagraph = styled.p`
  font-size: 12px;
  color: #4d556b;
  margin-bottom: 2px;
`;
export const TextTitle = styled.p`
  font-size: 12px;
  color: #90959e;
  margin-bottom: 2px;
`;
export const ImageSlip = styled.img`
  width: 100%;
  height: 7rem;
`;
