import React,{useContext} from "react";
import { Row, Col, Typography, Tag } from "antd";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { defaultCoverImage } from "src/data/data-initial-value";
import { IFormCoverImage } from "src/interfaces/interface-form";
const { Title, Text } = Typography;

interface IPropsHeader {
  header: string;
  subHeader: string;
}
export default function HeaderPage({ header, subHeader }: IPropsHeader) {
  const navigate = useNavigate();
  const {onSetToStore } = useContext(GlobalContext);

  const onClickBack = () => {
    onSetToStore({
      name: "coverImage",
      value: {
        ...defaultCoverImage,
      } as IFormCoverImage,
    });
    navigate(-1)
  }
  return (
    <div
      style={{ background: "#F9FAFB", padding: "10px 20px 10px 20px", borderRadius: "5px", }}
    >
      <Row align="middle">
        <Col xl={4}>
          <Tag
            style={{ cursor: "pointer", padding: 7,background: '#F1F4F5',border: 'unset' }}
            onClick={onClickBack}
          >
            <FontAwesomeIcon
              style={{ fontSize: 14, marginRight: 6, color: "#242424" }}
              icon={faArrowCircleLeft}
            />
            <Text >ย้อนกลับก่อนหน้า</Text>
          </Tag>
        </Col>
        <Col xl={20} style={{ textAlign: "end" }}>
          <Title level={4}>{header}</Title>
          <Text>{subHeader}</Text>
        </Col>
      </Row>
    </div>
  );
}
