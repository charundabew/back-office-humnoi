import React, { useState, useEffect, useContext } from "react";
import {
  BoxStyled,
  TitleForm,
  FormUploadCoverImage,
  HeaderSubPage,
  FooterActionForm,
} from "src/components";
import {
  Col,
  Form,
  Input,
  Radio,
  Row,
  Select,
  Spin,
  Modal,
  notification,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import styled from "styled-components";
import { useNavigate, useParams } from "react-router-dom";
import { USE_HOOK_SUB_CATEGORY } from "src/hooks";
import { ICategories, ISubCategory } from "src/interfaces/interface-response";
import {
  formProductLiquid,
  formProductSaltnic,
} from "src/data/data-initial-value";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { messageAction } from "src/utils/message";
import { AxiosError, AxiosResponse } from "axios";
import { API, AXIOS } from "src/services";
import {
  IFormCoverImage,
  IFormLiquid,
  IFormProductLiquid,
} from "src/interfaces/interface-form";
import { RcFile } from "antd/lib/upload";
import { convertToPrice } from "src/utils/pattern-number";
import { propsConfirmAddProduct } from "src/utils/props-component";
import { onKeyDownOnlyNumber, onKeyDownPrice } from "src/utils/helper";
const { Option } = Select;
const { confirm } = Modal;
const radioSweetItem = ["หวานน้อยมาก", "หวานน้อย", "หวานปานกลาง", "หวานมาก"];
const radioCoolItem = ["เย็นน้อยมาก", "เย็นน้อย", "เย็นปานกลาง", "เย็นมาก"];

export default function PageSaltnicProduct() {
  const { parentId, productId } = useParams();
  const [form] = Form.useForm();
  const navigator = useNavigate();
  const {
    coverImage,
    onSetToStore,
  }: { coverImage: IFormCoverImage; onSetToStore: any } =
    useContext(GlobalContext);
  const [optionsLiquid, setOptionsLiquid] = useState<ISubCategory[]>([]);
  const [totalValue, setTotalValue] = useState("");
  const [loading, setLoading] = useState(true);

  const onResetImage = () => {
    onSetToStore({
      name: "coverImage",
      value: {
        imagePath: "",
        file: {} as RcFile,
        base64: "",
        product_list_id: NaN,
      } as IFormCoverImage,
    });
  };
  const getProductId = () => {
    AXIOS({
      method: "get",
      url: API.PRODUCT_ID,
      params: {
        product_id: productId,
      },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: IFormProductLiquid = data.data;
        const firstItem: IFormLiquid =
          response.product_lists[0] ?? formProductLiquid;
        onSetToStore({
          name: "coverImage",
          value: {
            imagePath: response.image_path,
            file: {} as RcFile,
            base64: "",
            product_list_id: firstItem.id,
          } as IFormCoverImage,
        });
        const splitCoilSix: any[] = firstItem.coil_point_six?.split("/") ?? [
          "หวานน้อยมาก",
          "เย็นน้อยมาก",
        ];
        const splitCoilEight: any[] = firstItem.coil_point_eight?.split(
          "/"
        ) ?? ["หวานน้อยมาก", "เย็นน้อยมาก"];
        const radioCoile = {
          sweet_point_six: splitCoilSix[0] ?? "หวานน้อยมาก",
          sweet_point_eight: splitCoilEight[0] ?? "หวานน้อยมาก",
          cool_point_six: splitCoilSix[1] ?? "เย็นน้อยมาก",
          cool_point_eight: splitCoilEight[1] ?? "เย็นน้อยมาก",
        };

        const updateFormData: IFormLiquid = {
          ...firstItem,
          ...radioCoile,
          ...response,
          shipping_free: firstItem.free_shipping,
          smell: Number(firstItem.smell),
        };
        onUpdateTotal();
        form.setFieldsValue({
          ...updateFormData,
        });
        setLoading(false);
      })

      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });

    // Work with the response...
  };

  useEffect(() => {
    // declare the data fetching function
    const onPrepareForm = async () => {
      const response = await USE_HOOK_SUB_CATEGORY(parentId ?? "");
      const mapOptions =  response.flatMap((p:ICategories) => p.sub_categories.map((sub:ICategories) => sub ) )
      setOptionsLiquid(mapOptions);
      if (productId) {
        getProductId();
      } else {
        setLoading(false);
        form.setFieldsValue({ ...formProductSaltnic });
      }
    };
    // call the function
    onPrepareForm()
      // make sure to catch any error
      .catch(console.error);
  }, []);

  const onUpdateTotal = () => {
    const values = form.getFieldsValue();
    const { price, discount } = values;
    let result = Number(price ?? 0) - Number(discount ?? 0);
    setTotalValue(convertToPrice(result));
  };
  const onClickCancle = () => {
    form.resetFields();
    onResetImage()
    navigator(-1)
  };
  const validateMessages = {
    required: "ระบุข้อมูล ${label}",
  };

  const onFinish = (values: IFormLiquid) => {
    if (!coverImage.base64 && !coverImage.product_list_id) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description: "กรุณาอัปโหลดรูปภาพสินค้าก่อนบันทึกสินค้า",
      });
    } else {
      confirm({
        ...propsConfirmAddProduct,
        onOk() {
          return new Promise((resolve: any, reject: any) => {
            setLoading(true);
            const {
              price,
              stock,
              discount,
              smell,
              sweet_point_six,
              sweet_point_eight,
              cool_point_six,
              cool_point_eight,
              ml,
            } = values;
            const formData = new FormData();
            const obj = {
              ...values,
              id: productId === undefined ? null : Number(productId),
              price: Number(price),
              discount: Number(discount),
              stock: Number(stock),
              coil_point_eight: `${sweet_point_eight}/${cool_point_eight}`,
              coil_point_six: `${sweet_point_six}/${cool_point_six}`,
              ml: Number(ml),
              product_list_id: coverImage.product_list_id,
              smell
            };
            if (coverImage.base64) {
              formData.append("image", coverImage.file);
              formData.append("obj", JSON.stringify(obj));
            } else {
              formData.append("obj", JSON.stringify(obj));
            }

            AXIOS({
              method: productId === undefined ? "post" : "put",
              url: API.PRODUCTS, //ใช้เสน้เดียวกันคนบะ method
              data: formData,
            })
              .then(() => {
                messageAction("saveSuccess");
                onResetImage()
                navigator(-1);
                resolve();
              })
              .catch((error: AxiosError<any, any>) => {
                reject(error);
              });
          }).catch((error) => {
            messageAction("saveFail", error.message);
            setLoading(false);
          });
        },
      });
    }
  };
  return (
    <div>
      <HeaderSubPage
        header="เพิ่มรายการสินค้าแบบน้ำยา Saltnic"
        subHeader="สามารถเพิ่มรายการประเภทสินค้า เพิ่มได้ / ลบ / เพิ่มสินค้า"
      />
      <div className="App-paper-page">
        <Spin tip="กำลังโหลดข้อมูล..." spinning={loading}>
          <Form
            layout="vertical"
            scrollToFirstError
            form={form}
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <TitleForm type="form" />
            <BoxStyled color="white">
              <Row gutter={[16, 4]} justify="center">
                <Col xl={12} sm={12} xs={24}>
                  <Form.Item
                    required
                    label="ชื่อสินค้า"
                    name="name"
                    rules={[{ required: true }]}
                  >
                    <Input size="large" placeholder="ระบุชื่อสินค้า" />
                  </Form.Item>
                </Col>

                <Col xl={6} sm={12} xs={24}>
                  <Form.Item
                    required
                    label="รสชาติสินค้า"
                    name="category_id"
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="เลือกรสชาติสินค้า"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        (option!.children as unknown as string)
                          .toLowerCase()
                          .includes(input.toLowerCase())
                      }
                    >
                      {optionsLiquid.map((category: ISubCategory) => (
                        <Option key={category.id} value={category.id}>
                          {category.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xl={6} sm={12} xs={24}>
                  <Form.Item
                    required
                    label="ปริมาณสินค้า (ML)"
                    name="ml"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      size="large"
                      addonAfter="ml"
                      placeholder="สามารถระบุเป็นทศนิยมได้"
                      onKeyDown={onKeyDownPrice}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={12} xs={24}>
                  <Form.Item
                    label="จำนวนใน stock"
                    name="stock"
                    required
                    rules={[{ required: true }]}
                  >
                    <Input
                      size="large"
                      addonAfter="ชิ้น"
                      type="number"
                      placeholder="0"
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={12} xs={24}>
                  <Form.Item
                    required
                    label="ราคาสินค้า"
                    name="price"
                    rules={[{ required: true }]}
                  >
                    <Input
                      min={0}
                      type="number"
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={12} xs={24}>
                  <Form.Item label="ส่วนลด" name="discount">
                    <Input
                      type="number"
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={12} xs={24}>
                  <Form.Item label="ราคารวม">
                    <Input
                      value={totalValue}
                      disabled
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                    />
                  </Form.Item>
                </Col>

                <Col xl={24}>
                  <Form.Item required label="รายละเอียดสินค้า" name="detail">
                    <TextArea style={{ minHeight: "5rem" }} />
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item
                    name="sweet_point_six"
                    required
                    label="ระดับความหวานคอยล์ 0.6 "
                  >
                    <RadioGroupCustom>
                      {radioSweetItem.map((item: string) => (
                        <Radio key={item} value={item}>
                          {item}
                        </Radio>
                      ))}
                    </RadioGroupCustom>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item
                    required
                    name="cool_point_six"
                    label="ระดับความเย็นคอยล์ 0.6"
                  >
                    <RadioGroupCustom>
                      {radioCoolItem.map((item: string) => (
                        <Radio value={item} key={item}>
                          {item}
                        </Radio>
                      ))}
                    </RadioGroupCustom>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item
                    required
                    name="sweet_point_eight"
                    label="ระดับความหวานคอยล์ 0.8"
                  >
                    <RadioGroupCustom>
                      {radioSweetItem.map((item: string) => (
                        <Radio key={item} value={item}>
                          {item}
                        </Radio>
                      ))}
                    </RadioGroupCustom>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item
                    required
                    name="cool_point_eight"
                    label="ระดับความเย็นคอยล์ 0.8"
                  >
                    <RadioGroupCustom>
                      {radioCoolItem.map((item: string) => (
                        <Radio value={item} key={item}>
                          {item}
                        </Radio>
                      ))}
                    </RadioGroupCustom>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="shipping_free" required label="การจัดส่ง">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>มีค่าบริการจัดส่ง</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>จัดส่งฟรี</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="is_active" required label="สถานะสินค้า ">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>เปิดใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>ระงับการใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
            </BoxStyled>
            <TitleForm type="images" />
            <BoxStyled color="white">
              <Row gutter={[8, 8]}>
                <Col xl={12} sm={12} xs={24}>
                  <FormUploadCoverImage />
                </Col>
              </Row>
            </BoxStyled>
            <FooterActionForm onClickCancle={onClickCancle} />
          </Form>
        </Spin>
      </div>
    </div>
  );
}

const DivCheckBox = styled.div`
  padding: 12px 20px;
  border-radius: 5px;
  background: #f6f8fa;
`;
const RadioGroupCustom = styled(Radio.Group)`
  padding: 12px 20px;
  border-radius: 5px;
  background: #f6f8fa;
`;
const SpanCheckbox = styled.span`
  color: #46505e;
`;
