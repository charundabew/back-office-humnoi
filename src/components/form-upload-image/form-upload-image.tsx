import { InfoCircleFilled, LoadingOutlined } from "@ant-design/icons";
import {
  Col,
  Row,
  Typography,
  message,
  Form,
  Avatar,
  Input,
  Badge,
  Tooltip,
  Popconfirm,
} from "antd";
import Dragger from "antd/lib/upload/Dragger";
import React, { useContext, useState } from "react";
import styled from "styled-components";
import IconAddImage from "src/assets/icons/add-image.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { IFormImageProduct } from "src/interfaces/interface-form";
import { RcFile } from "antd/lib/upload";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { renderImage } from "src/utils/helper";

const { Text } = Typography;
const newUpload = {
  image: "",
  product_id: NaN,
  color: "",
  stock: 0,
  base64: "",
  uid: "",
};
export default function FormUploadImage({ onUpdateDelete }: any) {
  const { onSetUploaded, uploadedImage } = useContext(GlobalContext);
  const [uploadImage, setUploadImage] = useState(false);
  const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result as string));
    reader.readAsDataURL(img);
  };

  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("กรุณาอัพโหลดเฉพาะไฟล์รูปภาพเท่านั้น");
    } else {
      setUploadImage(true);
      setTimeout(() => {
        getBase64(file as RcFile, (url) => {
          const obj: IFormImageProduct = {
            ...newUpload,
            image: file,
            base64: url,
            uid: file?.uid,
          };
          onSetUploaded([...uploadedImage, obj]);
        });
        setUploadImage(false);
      }, 500);
    }
  };
  const onChangeForm = (name: string, index: number, value: any) => {
    const tmp: any[] = uploadedImage;
    tmp[index][name] = value;
    onSetUploaded([...tmp]);
  };
  const onDeleteItem = (index: number, id: number | undefined) => {
    const tmp: IFormImageProduct[] = uploadedImage;
    tmp.splice(index, 1);
    onSetUploaded(tmp);
    if (id) {
      onUpdateDelete(id);
    }
  };

  return (
    <div>
      <Row gutter={[16, 8]}>
        <Col xl={24}>
          <TextTitle strong>รูปภาพตัวเลือกสินค้า</TextTitle>
          <DivTextIcon>
            <InfoCircleFilled />
            <TextInfo type="secondary">
              อัปโหลดรูปภาพจำนวน ไม่เกิน 10 รูปภาพ
            </TextInfo>
          </DivTextIcon>
        </Col>
        <Col xl={8} sm={12} xs={24}>
          <DraggerStyle
            showUploadList={false}
            beforeUpload={beforeUpload}
            multiple={false}
          >
            {uploadImage ? (
              <div>
                <LoadingOutlined style={{ fontSize: 36, color: "#5c68ff" }} />
                <DivText>
                  <TextHightLight>กำลังอัปโหลดรูปภาพ</TextHightLight>
                </DivText>
                <DivTextSub>
                  <TextGrey type="secondary">กรุณารอสักครู่...</TextGrey>
                </DivTextSub>
              </div>
            ) : (
              <div>
                <Avatar shape="square" size={48} src={IconAddImage}></Avatar>
                <DivText>
                  <TextHightLight>Upload a file</TextHightLight>
                  <TextGrey type="secondary">or drag and drop</TextGrey>
                </DivText>
                <DivTextSub>
                  <TextGrey type="secondary">PNG, JPG, GIF up tp 10MB</TextGrey>
                </DivTextSub>
              </div>
            )}
          </DraggerStyle>
        </Col>
        {uploadedImage.map((item: IFormImageProduct, index: number) => (
          <Col xl={8} sm={12} xs={24} key={index}>
            <Badge
              offset={[0, 0]}
              count={
                <Popconfirm
                  title="ต้องการลบสินค้าชิ้นนี้?"
                  onConfirm={() => onDeleteItem(index, item.id)}
                  okText="ตกลง"
                  cancelText="ยกเลิก"
                >
                  <FontAwesomeIcon
                    icon={faTimesCircle}
                    style={{
                      fontSize: "20px",
                      color: "#4d5fac",
                      cursor: "pointer",
                    }}
                  />
                </Popconfirm>
              }
            >
              <DivUploadCover>
                <Row gutter={[16, 8]} align="middle">
                  <Col xl={8}>
                    <ImageCover
                      src={item.base64 ? item.base64 : renderImage(item.image)}
                    ></ImageCover>
                  </Col>
                  <Col xl={16}>
                    <Form layout="vertical">
                      <Tooltip
                        placement="rightBottom"
                        title="ชื่อสีสินค้าต้องไม่ซ้ำกัน"
                      >
                        <Form.Item
                          required
                          label="ระบุข้อมูลสินค้าให้ครบถ้วน"
                          style={{
                            padding: "0 0 1px",
                          }}
                        >
                          <Input
                            size="large"
                            placeholder="ระบุสีสินค้า"
                            value={item.color}
                            onChange={(e: any) =>
                              onChangeForm("color", index, e.target.value)
                            }
                          />
                        </Form.Item>
                      </Tooltip>
                      <Tooltip
                        placement="rightBottom"
                        title="ระบุเป็นตัวเลข 0-9"
                      >
                        <Form.Item required style={{ marginBottom: "5px" }}>
                          <Input
                            type="number"
                            value={item.stock}
                            size="large"
                            addonAfter="ชิ้น"
                            placeholder="จำนวนสินค้าที่มีใน stock"
                            onChange={(e: any) =>
                              onChangeForm("stock", index, e.target.value)
                            }
                          />
                        </Form.Item>
                      </Tooltip>
                    </Form>
                  </Col>
                </Row>
              </DivUploadCover>
            </Badge>
          </Col>
        ))}
      </Row>
    </div>
  );
}

const DivText = styled.div`
  margin-top: 1rem;
`;
const ImageCover = styled.img`
  height: 6rem;
  width: 100%;
  border-radius: 5px;
`;
const TextTitle = styled(Text)`
  font-size: 14px;
`;
const TextInfo = styled(Text)`
  margin-left: 10px;
`;
const DivTextIcon = styled.div`
  color: #90959e;
  margin-top: 5px;
`;
const DivTextSub = styled.div`
  color: #90959e;
  margin-top: 0.1rem;
`;
const DraggerStyle = styled(Dragger)`
  &.ant-upload.ant-upload-drag {
    border: 1px dashed #90959e;
    border-radius: 10px;
    background: white;
    height: 144px;
  }
`;
const DivUploadCover = styled.div`
  padding: 20px 18px;
  border: 1px dashed #90959e;
  border-radius: 10px;
  background: white;
`;
const TextHightLight = styled.span`
  color: #5151f9;
  font-weight: 500;
  font-size: 14px;
`;
const TextGrey = styled(Text)`
  font-size: 12px;
  margin-left: 10px;
`;
