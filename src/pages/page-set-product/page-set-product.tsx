import React, { useState, useEffect, useContext } from "react";
import {
  BoxStyled,
  TitleForm,
  HeaderSubPage,
  FormAddProductSet,
  ButtonCustom,
} from "src/components";
import {
  Col,
  Form,
  Input,
  Radio,
  Row,
  Select,
  Card,
  Typography,
  Spin,
  Steps,
  Upload,
  message,
  Modal,
  notification,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import styled from "styled-components";
import DrawerSelectedProduct from "./drawer-selected-product";
import { useNavigate, useParams } from "react-router-dom";
import {
  IFormCoverImage,
  IFormProductSet,
  IOptionsStep1,
  IProduct,
  IProductInSet,
  IPropsDrawerSelected,
} from "src/interfaces/interface-form";
import {
  USE_HOOK_BRAND,
  USE_HOOK_CATEGORY,
  USE_HOOK_PRODUCT_SET_ID,
} from "src/hooks";
import {
  IBrands,
  ICategories,
  ICategory,
} from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import { messageAction } from "src/utils/message";
import { AxiosError } from "axios";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { IRowsProduct } from "src/interfaces/interface-table";
import _ from "lodash";
import { RcFile } from "antd/lib/upload";
import { srcPlacholder } from "src/mock/mock-img-src";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-regular-svg-icons";
import {
  defaultCoverImage,
  formDataProductSet,
} from "src/data/data-initial-value";
import { convertToPrice } from "src/utils/pattern-number";
import { LoadingOutlined } from "@ant-design/icons";
import { onKeyDownOnlyNumber, renderImage } from "src/utils/helper";
import { propsConfirmAddProduct } from "src/utils/props-component";
const { Option } = Select;
const { Text } = Typography;
const { Step } = Steps;
const { confirm } = Modal;
const gridEnabledStyle: React.CSSProperties = {
  width: "25%",
  textAlign: "center",
  background: "#F8F9FE",
  padding: "10px",
  color: "#4D5FAC",
  cursor: "pointer",
};

const gridDisabledStyle: React.CSSProperties = {
  width: "25%",
  textAlign: "center",
  background: "#FAFAFA",
  color: "#D7D7D7",
  padding: "10px",
  cursor: "not-allowed",
};
export default function PageSetproduct() {
  const [currentStep, setCurrentStep] = useState(0);
  const { selectedProduct, onSetToStore } = useContext(GlobalContext);
  const [uploadImage, setUploadImage] = useState(false);
  const [form] = Form.useForm();
  const navigator = useNavigate();
  const { id } = useParams();
  const [totalValue, setTotalValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [options, setOptions] = useState<IOptionsStep1>({
    optionsBrand: [],
    optionsCatergory: [],
  });

  const [propsDrawer, setPropsDrawer] = useState<IPropsDrawerSelected>({
    isOpen: false,
    category: {} as ICategory,
    productType: "",
  });
  const [imageFile, setImageFile] = useState<IFormCoverImage>({
    imagePath: "",
    base64: "",
    file: {} as RcFile,
    product_list_id: NaN,
  });
  const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result as string));
    reader.readAsDataURL(img);
  };

  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("กรุณาอัปโหลดเฉพาะไฟล์รูปภาพเท่านั้น");
    } else {
      getBase64(file as RcFile, (url) => {
        setUploadImage(true);
        setTimeout(() => {
          setImageFile({
            file: file,
            base64: url,
            imagePath: "",
            product_list_id: undefined,
          });
          setUploadImage(false);
        }, 500);
      });
    }
    // const isLt2M = file.size / 1024 / 1024 < 2;
    // if (!isLt2M) {
    //   message.error("Image must smaller than 2MB!");
    // }
  };
  const onClickCardCat = (item: ICategory) => {
    setPropsDrawer({
      isOpen: true,
      category: item,
      productType: currentStep === 1 ? "main" : "secondary",
    });
  };
  useEffect(() => {
    // declare the data fetching function
    const onPrepareForm = async () => {
      const responseBrands = await USE_HOOK_BRAND();
      const responseCategory = await USE_HOOK_CATEGORY();
      setOptions({
        optionsBrand: responseBrands,
        optionsCatergory: responseCategory.filter(
          (item: ICategories) => item.id <= 11
        ),
      });
      if (id === undefined) {
        form.setFieldsValue({
          ...formDataProductSet,
        });
        onSetToStore({ name: "selectedProduct", value: [] });
        setImageFile(defaultCoverImage);
        setLoading(false);
      } else {
        const response: IFormProductSet = await USE_HOOK_PRODUCT_SET_ID(id);
        form.setFieldsValue({ ...response });
        const productSet: IProductInSet[] = response.product_in_set ?? [];
        const defaultProduct = productSet.flatMap(
          (item: IProductInSet) => item.products
        );
        onSetToStore({
          name: "selectedProduct",
          value: defaultProduct,
        });
        setImageFile({
          imagePath: response.image_file,
          file: {} as RcFile,
          base64: "",
          product_list_id: response.main_product_id,
          //เช็คอีกทีว่าใช้ค่าอะไร
        });
        onUpdateTotal();
        setLoading(false);
      }
    };
    // call the function
    onPrepareForm();
  }, [id]);
  const onUpdateTotal = () => {
    const values = form.getFieldsValue();
    const { qty, price, discount } = values;
    let result = Number(qty ?? 1) * Number(price ?? 0) - Number(discount ?? 0);
    setTotalValue(convertToPrice(result));
  };

  const validateMessages = {
    required: "ระบุข้อมูล ${label}",
  };

  const onFinish = (values: IFormProductSet) => {
    const mainItemIsExist = selectedProduct.every((item: IProduct) =>
      [0, 1, 2, 3].every((main: number) => item.root_parent_id !== main)
    );
    if (!imageFile.base64 && !imageFile.product_list_id) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description: "กรุณาอัปโหลดรูปภาพสินค้าก่อนบันทึกสินค้า",
      });
    } else if (selectedProduct.length === 0) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description: "กรุณาเพิ่มสินค้าในเซ็ต",
      });
    } else if (mainItemIsExist) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description: "กรุณาเพิ่มสินค้าประเภท'สินค้าหลัก' อย่างน้อย 1 ชิ้น",
      });
    } else {
      confirm({
        ...propsConfirmAddProduct,
        onOk() {
          return new Promise((resolve: any, reject: any) => {
            setLoading(true);
            const mergeItem = [...selectedProduct];
            const items = _.groupBy(mergeItem, "category_id");
            const mapProductInSet: IProductInSet[] = [];
            _.forIn(items, (value: IRowsProduct[], key: any) => {
              mapProductInSet.push({
                product_id: value.map((item: IRowsProduct) => item.id),
                category_id: Number(key),
                freebies: value[0].freebies ?? true,
              });
            });
            const mainProduct = selectedProduct[0];
            const { price, discount, qty } = values;
            const obj = {
              ...values,
              id: Number(id) || null,
              main_product_id: mainProduct.id,
              main_category_id: mainProduct.category_id,
              price: Number(price),
              discount: Number(discount),
              qty: Number(qty),
              product_in_set: mapProductInSet,
              product_list_id: imageFile.product_list_id,
            };
            const formData = new FormData();
            if (imageFile.base64) {
              formData.append("image", imageFile.file);
              formData.append("obj", JSON.stringify(obj));
            } else {
              formData.append("obj", JSON.stringify(obj));
            }
            AXIOS({
              method: id === undefined ? "post" : "put",
              url: API.STEP_FORM_SET,
              data: formData,
            })
              .then(() => {
                messageAction("saveSuccess");
                navigator(-1);
                resolve();
              })
              .catch((error: AxiosError<any, any>) => {
                reject(error);
              });
          }).catch((error) => {
            messageAction("saveFail", error.message);
            setLoading(false);
          });
        },
      });
    }
  };
  const onClickCancle = () => {
    navigator(-1);
  };
  const onDrawerClose = () => {
    setPropsDrawer({
      isOpen: false,
      category: {} as ICategory,
      productType: "",
    });
  };

  const onChangeStep = (value: number) => {
    setCurrentStep(value);
  };
  const checkMainItem = (itemId: number) => {
    const isMainProduct = [1, 2, 3].some((id: number) => itemId === id);
    return isMainProduct;
  };
  // const checkSecondaryItem = (itemId: number) => {
  //   const isSecondayProduct = [6, 7, 8, 9].some((id: number) => itemId === id);
  //   return isSecondayProduct;
  // };
  const checkIsAvalible = (cateId: number) => {
    const mainProduct: IProduct = selectedProduct[0] ?? {
      category_id: NaN,
      root_parent_id: 0,
    };
    const parentId = mainProduct.root_parent_id || mainProduct.category_id;
    if ([1, 2, 3].some((id: number) => id === cateId)) {
      return cateId === parentId;
    } else {
      return selectedProduct.some((item: IRowsProduct) => {
        const parentIdSecondary = item.root_parent_id || item.category_id;
        return parentIdSecondary === cateId;
      });
    }
  };
  return (
    <div>
      <HeaderSubPage
        header="เพิ่มรายการสินค้าแบบเซ็ตพร้อมสูบ"
        subHeader="สามารถเพิ่มรายการประเภทสินค้า เพิ่มได้ / ลบ / เพิ่มสินค้า"
      />
      <div className="App-paper-page">
        <Spin spinning={loading} tip="กำลังโหลดข้อมูล...">
          <Form
            scrollToFirstError
            form={form}
            layout="vertical"
            onFinish={onFinish}
            validateMessages={validateMessages}
          >
            <TitleForm type="form" />
            <BoxStyled color="white">
              <Row gutter={[20, 4]} justify="center">
                <Col xl={24} sm={24} xs={24}>
                  <Form.Item
                    name="name"
                    label="ชื่อสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input size="large" placeholder="ระบุชื่อสินค้า" />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item
                    name="brand_id"
                    required
                    label="แบรนด์สินค้า"
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="เลือกแบรนด์สินค้า"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        (option!.children as unknown as string)
                          .toLowerCase()
                          .includes(input.toLowerCase())
                      }
                    >
                      {options.optionsBrand.map((brand: IBrands) => (
                        <Option key={brand.id} value={brand.id}>
                          {brand.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item
                    name="price"
                    required
                    label="ราคาสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item name="discount" label="ส่วนลด">
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={6} sm={4} xs={24}>
                  <Form.Item label="ราคารวม">
                    <Input
                      disabled
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                      value={totalValue}
                    />
                  </Form.Item>
                </Col>
                <Col xl={24}>
                  <Form.Item name="detail" label="รายละเอียดสินค้า">
                    <TextArea style={{ minHeight: "5rem" }} />
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="free_shipping" required label="การจัดส่ง">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>มีค่าบริการจัดส่ง</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>จัดส่งฟรี</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="is_active" required label="สถานะสินค้า ">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>เปิดใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>ระงับการใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
            </BoxStyled>
            <TitleForm type="images" />
            <BoxStyled color="white">
              <DivTextIcon>
                <DivAddBoxSet>
                  <Row gutter={[8, 8]} align="middle">
                    <Col xl={10}>
                      <Steps
                        direction="vertical"
                        size="small"
                        current={currentStep}
                        onChange={onChangeStep}
                      >
                        <Step
                          title="เพิ่มรูปภาพหน้าปก"
                          description={
                            <TextInfo type="secondary">
                              ตั้งค่ารูปหน้าปกสินค้า
                            </TextInfo>
                          }
                        ></Step>
                        <Step
                          title={
                            <Text style={{ fontSize: 14 }}>
                              เพิ่มสินค้าหลักในเซ็ต
                              <span style={{ color: "red", marginLeft: "5px" }}>
                                *
                              </span>
                            </Text>
                          }
                          description={
                            <TextInfo type="secondary">
                              เพิ่มสินค้าหลัก 1 ชิ้น จากตัวเลือก
                            </TextInfo>
                          }
                        ></Step>
                        <Step
                          title="สินค้าในเซ็ต"
                          description={
                            <TextInfo type="secondary">
                              เพิ่มสินค้าอย่างน้อย 2 ชิ้นเพื่อสร้างเซ็ตพร้อมสูบ
                            </TextInfo>
                          }
                        />
                      </Steps>
                    </Col>
                    <Col xl={14}>
                      {currentStep === 0 ? (
                        <DivUploadCover>
                          <Row
                            style={{ height: "100%" }}
                            gutter={[4, 4]}
                            align="middle"
                            justify="center"
                          >
                            <Col xl={7} style={{ textAlign: "center" }}>
                              {uploadImage ? (
                                <LoadingOutlined
                                  style={{ fontSize: 36, color: "#5c68ff" }}
                                />
                              ) : (
                                <Upload
                                  showUploadList={false}
                                  beforeUpload={beforeUpload}
                                  multiple={false}
                                >
                                  <ImageCover
                                    style={{ cursor: "pointer" }}
                                    src={
                                      imageFile.imagePath
                                        ? renderImage(imageFile.imagePath)
                                        : imageFile.base64
                                        ? imageFile.base64
                                        : srcPlacholder
                                    }
                                  ></ImageCover>
                                </Upload>
                              )}
                            </Col>
                            <Col xl={12} style={{ textAlign: "center" }}>
                              <Text type="secondary">
                                {imageFile.file.name ||
                                  "กรุณาอัปโหลดรูปภาพหน้าปกสินค้า"}
                              </Text>
                            </Col>
                            <Col xl={5}>
                              <Upload
                                showUploadList={false}
                                beforeUpload={beforeUpload}
                                multiple={false}
                              >
                                <ButtonCustom
                                  icon="upload"
                                  type="default"
                                  text="เปลี่ยนรูป"
                                />
                              </Upload>
                            </Col>
                          </Row>
                        </DivUploadCover>
                      ) : currentStep === 1 ? (
                        <Card style={{ color: "#4D5FAC", cursor: "pointer" }}>
                          {options.optionsCatergory.map((item: ICategory) => (
                            <Card.Grid
                              key={item.id}
                              hoverable={checkMainItem(item.id)}
                              onClick={() =>
                                checkMainItem(item.id)
                                  ? onClickCardCat(item)
                                  : null
                              }
                              style={
                                checkMainItem(item.id)
                                  ? gridEnabledStyle
                                  : gridDisabledStyle
                              }
                            >
                              <div>
                                {checkIsAvalible(item.id) ? (
                                  <FontAwesomeIcon
                                    color={
                                      checkMainItem(item.id)
                                        ? "green"
                                        : "#d7d7d7"
                                    }
                                    icon={faCheckCircle}
                                  />
                                ) : null}
                                <span> {item.name}</span>
                              </div>
                            </Card.Grid>
                          ))}
                        </Card>
                      ) : (
                        <Card style={{ color: "#4D5FAC", cursor: "pointer" }}>
                          {options.optionsCatergory.map((item: ICategory) => (
                            <Card.Grid
                              key={item.id}
                              onClick={() =>
                                !checkMainItem(item.id)
                                  ? onClickCardCat(item)
                                  : null
                              }
                              style={
                                !checkMainItem(item.id)
                                  ? gridEnabledStyle
                                  : gridDisabledStyle
                              }
                            >
                              <div>
                                {checkIsAvalible(item.id) ? (
                                  <FontAwesomeIcon
                                    color={
                                      !checkMainItem(item.id)
                                        ? "green"
                                        : "#d7d7d7"
                                    }
                                    icon={faCheckCircle}
                                  />
                                ) : null}
                                <span> {item.name}</span>
                              </div>
                            </Card.Grid>
                          ))}
                        </Card>
                      )}
                    </Col>
                  </Row>
                </DivAddBoxSet>
              </DivTextIcon>
            </BoxStyled>
            <TitleForm type="productSet" />
            <BoxStyled color="white">
              <FormAddProductSet />
            </BoxStyled>
            <div style={{ marginTop: "1rem" }}>
              <Row justify="space-between">
                <Col xl={3}>
                  <ButtonCustom
                    width="100%"
                    icon="cancel"
                    onClick={onClickCancle}
                    type="ghost"
                    text="ยกเลิกข้อมูล"
                  />
                </Col>
                <Col xl={3}>
                  <Form.Item>
                    <ButtonCustom
                      htmlType="submit"
                      width="100%"
                      icon="save"
                      type="primary"
                      text="บันทึกข้อมูล"
                    />
                  </Form.Item>
                </Col>
              </Row>
            </div>
          </Form>
        </Spin>
      </div>
      <DrawerSelectedProduct
        onClose={onDrawerClose}
        propsDrawer={propsDrawer}
      />
    </div>
  );
}

const ImageCover = styled.img`
  height: 7rem;
  width: 100%;
  border-radius: 10px;
`;

const TextInfo = styled(Text)`
  margin-left: 10px;
`;
const DivTextIcon = styled.div`
  color: #90959e;
`;
const DivUploadCover = styled.div`
  padding: 18px;
  border: 1px dashed #90959e;
  border-radius: 10px;
  background: white;
  height: 9rem;
`;

const DivCheckBox = styled.div`
  padding: 12px 20px;
  border-radius: 5px;
  background: #f6f8fa;
`;
const SpanCheckbox = styled.span`
  color: #46505e;
`;
const DivAddBoxSet = styled.div`
  padding: 15px;
  border: 1px dashed #90959e;
  border-radius: 10px;
  background: white;
  text-align: center;
`;
