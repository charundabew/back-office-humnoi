import { AxiosResponse } from "axios";
import { AXIOS, API } from "src/services";

const USE_HOOK_CATEGORY = async () => {
  try {
    let { data }: AxiosResponse<any, any> = await AXIOS({
      method: "get",
      url: API.CATEGORY,
    });
    return data.data;
    // Work with the response...
  } catch (err: any) {
    // Handle error
    return [];
  }
};

export default USE_HOOK_CATEGORY;
