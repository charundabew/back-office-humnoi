import { Select } from "antd";
import styled from "styled-components";

export const SelectStyled = styled(Select)`
  width: 100%;
  &.ant-select:not(.ant-select-customize-input) .ant-select-selector {
    height: 35px !important;
    border-radius: 5px !important;
    font-size: 12px;
  }
`;
