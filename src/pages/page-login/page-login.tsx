import { LockOutlined, UserOutlined } from "@ant-design/icons";
import {
  Button,
  notification,
  Checkbox,
  Form,
  Input,
  message,
  Spin,
} from "antd";
import React, { useState } from "react";
import ImageShopping from "src/assets/images/online-shopping.jpg";
import styled from "styled-components";
import { HOST } from "src/services";
import axios, { AxiosResponse } from "axios";
import { IResLogin } from "src/interfaces/interface-response";
import { useNavigate } from "react-router-dom";

const AXIOS_API = axios.create({
  baseURL: HOST,
});

export default function PageLogin() {
  const navigator = useNavigate();
  const [loading, setLoading] = useState(false);

  const onFinishLogin = (form: any) => {
    setLoading(true);
    AXIOS_API({
      method: "post",
      data: form,
      url: "/login",
    })
      .then((res: AxiosResponse<any, any>) => {
        const data: IResLogin = res.data.data;
        const setToken = new Promise((resolve) => {
          setLoading(false);
          localStorage.setItem("username", data.username);
          localStorage.setItem("name", data.name);
          resolve(localStorage.setItem("access_token", data.access_token));
        });
        const GotoPage = () => {
          return navigator("/admin");
        };
        message
          .success("เข้าสู่ระบบสำเร็จ", 0.5)
          .then(() => setToken.then(GotoPage));
      })
      .catch((error: any) => {
        setLoading(false);
        if (error.response) {
          const err = error.response.data;
          if (err.message.includes("user")) {
            notification.error({
              message: "ไม่พบผู้ใช้งาน",
              description: "กรุณาตรวจสอบหมาย ชื่อผู้ใช้ (username) อีกครั้ง",
            });
          } else if (
            err.message === "Invalid login credentials. Please try again"
          ) {
            notification.error({
              message: "รหัสผ่านไม่ถูกต้อง",
              description: "กรุณาตรวจสอบหมาย รหัสผ่าน (password) อีกครั้ง",
            });
          } else {
            notification.error({
              message: "เกิดข้อผิดพลาด!",
              description: "กรุณาลองอีกครั้ง",
            });
          }
        } else {
          notification.warning({
            message: "การเชื่อมต่อขัดข้อง!",
            description:
              "กรุณาตรวจสอบการเชื่อมต่ออินเตอร์เน็ต ก่อนการเข้าใช้งาน",
          });
        }
      });
  };
  return (
    <DivRoot>
      <CardStyle>
        <img
          src={ImageShopping}
          width="100%"
          style={{ position: "relative" }}
          alt="login"
        />
        <div style={{ padding: "1rem 1rem 0rem 1rem" }}>
          <Spin spinning={loading} tip="กำลังเข้าสู่ระบบ...">
            <FormStyle
              name="basic"
              onFinish={onFinishLogin}
              // onFinishFailed={onFinishFailed}
              autoComplete="off"
              layout="vertical"
            >
              <FormItemStyle
                name="username"
                label="ชื่อผู้ใช้งาน (username)"
                rules={[
                  { required: true, message: "กรุณาระบุชื่อผู้เข้าใช้งาน!" },
                ]}
              >
                <InputStyle
                  prefix={<UserOutlined style={{ color: "#5E40A7" }} />}
                />
              </FormItemStyle>

              <FormItemStyle
                name="password"
                label="รหัสผ่าน (password)"
                rules={[
                  {
                    required: true,
                    message: "กรุณาระบุรหัสผ่านเพื่อเข้าใช้งาน!",
                  },
                ]}
              >
                <InputPasswordStyle
                  prefix={<LockOutlined style={{ color: "#5E40A7" }} />}
                />
              </FormItemStyle>
              <Form.Item>
                <a style={{ float: "right" }} href="">
                  ลืมรหัสผ่าน
                </a>
              </Form.Item>
              <Form.Item>
                <Button
                  style={{ width: "100%", borderRadius: 5 }}
                  type="primary"
                  htmlType="submit"
                >
                  เข้าสู่ระบบ
                </Button>
              </Form.Item>
            </FormStyle>
          </Spin>
        </div>
      </CardStyle>
    </DivRoot>
  );
}

const CardStyle = styled.div`
  position: absolute;
  left: 50%;
  transform: translate(-50%, 10%);
  padding: 20px;
  border-radius: 30px;
  background: hsl(213deg 85% 97%);
  box-shadow: 0 0 1em hsl(231deg 40% 40%);
  width: 27rem;
`;
const DivRoot = styled.div`
  height: 100vh;
  background-image: url("./bg-login.jpg");
`;
const FormStyle = styled(Form)`
  .ant-form-item-explain-error {
    margin-top: 6px;
    font-size: 12px !important;
    color: #5151f9;
  }
  .ant-input-affix-wrapper-status-error:not(.ant-input-affix-wrapper-disabled):not(.ant-input-affix-wrapper-borderless).ant-input-affix-wrapper,
  .ant-input-affix-wrapper-status-error:not(.ant-input-affix-wrapper-disabled):not(.ant-input-affix-wrapper-borderless).ant-input-affix-wrapper:hover {
    border-color: #5151f9;
  }
`;
const FormItemStyle = styled(Form.Item)`
  text-align: left;
  .ant-form-item-label > label {
    font-size: 14px;
    color: #5e40a7 !important;
  }
`;
const InputStyle = styled(Input)`
  border: 1px solid #a98bef;
  opacity: 0.6;
`;
const InputPasswordStyle = styled(Input.Password)`
  border: 1px solid #a98bef;
  opacity: 0.6;
`;
