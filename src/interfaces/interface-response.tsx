import { IFormUsers } from "./interface-form";
import { IRowsProduct } from "./interface-table";

export interface IResLogin {
  access_token: string;
  expires_in: Date;
  access_path: "/owner" | string;
  username: string;
  name: string;
}

export interface IBrands {
  id: number;
  name: string;
  image_file: string;
}
export interface ITableProduct {
  total_rows: number;
  limit: number;
  page: number;
  search?: string;
  rows: IRowsProduct[];
}
export interface ITableProductMain {
  total_rows: number;
  rows: IRowsProduct[];
}

export interface ITableUser {
  total_rows: number;
  limit: number;
  page: number;
  search?: string;
  rows: IFormUsers[];
}


export interface ICategory {
  //อันเก่า
  id: number;
  name: string;
  is_active: boolean;
  created_at?: Date;
  updated_at?: Date;
}

export interface ICategories {
  id: number;
  parent_id: number;
  name: string;
  is_active: boolean;
  sub_categories: any[];
  created_at?: Date;
  updated_at?: Date;
}
export interface ISubCategory {
  id?: number;
  name: string;
  parent_id?: number;
  is_active: boolean;
}

export interface INewSubCategory {
  name: string;
  is_active: boolean;
  parent_id: number;
}
export interface IResStepFirst {
  brand_id: number;
  category_id: number;
  product_id: number;
}
export interface IOrderStatus {
  id: number;
  status_th: string;
  created_at: Date;
  updated_at: Date | null;
  deleted_at: Date | null;
}

export interface IBanners {
  id: number;
  index: number;
  img_path: string;
}
