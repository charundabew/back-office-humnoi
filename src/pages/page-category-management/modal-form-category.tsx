import React, { useState, useEffect } from "react";
import { Col, Input, Row, Spin, Switch, Typography } from "antd";
import { ButtonCustom } from "src/components";
import { EditFilled } from "@ant-design/icons";
import { ModalFormStyled } from "src/styles/modal-styled";
import { ICategories } from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import { messageAction } from "src/utils/message";
import { AxiosError } from "axios";

const { Text } = Typography;

interface IPropsModalCaergory {
  onCloseModal: () => void;
  modalProps: {
    visible: boolean;
    propsFormItem: ICategories;
    type: "add-child" | "edit-parent" | string;
  };
  onFinishedModal: () => void;
}
interface IPropsForm {
  id: number;
  name: string;
  is_active: boolean;
  parent_id: number;
}
export default function ModalFormCategory({
  onCloseModal,
  onFinishedModal,
  modalProps: { visible, propsFormItem, type },
}: IPropsModalCaergory) {
  const [loading, setLoading] = useState(false);
  const [formItem, setFormItem] = useState<IPropsForm>({
    id: NaN,
    name: "",
    is_active: true,
    parent_id: NaN,
  });

  const onSetDefault = () => {
    const { id, name, is_active, parent_id } = propsFormItem;
    setFormItem({
      id,
      name,
      is_active,
      parent_id,
    });
  };
  useEffect(onSetDefault, [visible]);
  const onHandleChange = (name: string, value: string | boolean) => {
    setFormItem({ ...formItem, [name]: value });
  };

  const onSubmitData = () => {
    setLoading(true);
    AXIOS({
      method: "put",
      url: API.CATEGORY,
      params: formItem,
    })
      .then(() => {
        messageAction("saveSuccess");
        setLoading(false);
        onFinishedModal();
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
        messageAction("saveFail", error.message);
      });
  };
  
  return (
    <div>
      <ModalFormStyled
        width={400}
        title={"แก้ไขประเภทสินค้า"}
        visible={visible}
        onCancel={onCloseModal}
        footer={
          <ButtonCustom
            key="submit"
            onClick={onSubmitData}
            type="primary"
            icon="save"
            text="บันทึกรายการ"
          />
        }
      >
        <Spin spinning={loading} tip="กำลังบันทึกข้อมูล...">
          <Row gutter={[8, 16]}>
            <Col xl={24}>
              <div style={{ marginBottom: "10px" }}>
                <Text>ชื่อประเภทสินค้า</Text>
                <Text style={{ marginLeft: "6px" }} type="secondary">
                  (ความยาวไม่เกิน 50 ตัวอักษร)
                </Text>
              </div>
              <Input
                value={formItem.name}
                suffix={<EditFilled />}
                style={{ width: "100%" }}
                onChange={(e: any) => onHandleChange("name", e.target.value)}
              />
            </Col>
            <Col span={8}>
              <Text>สถานะการเปิดใช้งาน</Text>
            </Col>
            <Col xl={14}>
              <Switch
                checked={formItem.is_active}
                onChange={(e: any) => onHandleChange("is_active", e)}
              />
            </Col>
          </Row>
        </Spin>
      </ModalFormStyled>
    </div>
  );
}
