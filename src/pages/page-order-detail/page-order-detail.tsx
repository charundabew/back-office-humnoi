import React, { useEffect, useState } from "react";
import { Row, Col, Modal, Spin, Skeleton } from "antd";
import { HeaderSubPage } from "src/components";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { useParams } from "react-router-dom";
import { IConfirmShipment, IOrderDetail } from "src/interfaces/interface-order";
import { formOrderDetail } from "src/data/data-initial-value";
import { OrderListItem, OrderPayment, OrderShipment } from "./components";
import { LoadingOutlined } from "@ant-design/icons";
import {
  propsConfirmShipment,
  propsModalApprovePayment,
  propsModalDeniedPayment,
} from "src/utils/props-component";
import { messageAction } from "src/utils/message";
import dayjs from "dayjs";
import { DivRoot } from "./components/styled-component-page";
const { confirm } = Modal;

export default function PageOrderDetail() {
  const { code } = useParams();
  const [loading, setLoading] = useState(true);
  const [orderDetail, setOrderDetail] = useState<IOrderDetail>(formOrderDetail);

  const onFetchData = () => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.ORDER_DETAIL,
      params: {
        ref_code: code,
      },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: IOrderDetail = data.data;
        setOrderDetail(response);
        setLoading(false);
      })

      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });
  };
  useEffect(onFetchData, []);

  const onApprovePayment = (is_approve: boolean) => {
    const propsConfirm = is_approve
      ? propsModalApprovePayment
      : propsModalDeniedPayment;
    confirm({
      ...propsConfirm,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          setLoading(true);
          AXIOS({
            method: "put",
            url: API.APPROVE_PAYMENT,
            params: { order_id: orderDetail.id, is_approve },
          })
            .then(() => {
              messageAction("saveSuccess");
              onFetchData();
              resolve();
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          setLoading(false);
          messageAction("saveFail");
        });
      },
    });
  };
  const onConfirmShipping = (values: IConfirmShipment) => {
    confirm({
      ...propsConfirmShipment,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          setLoading(true);
          AXIOS({
            method: "post",
            url: API.CONFIRM_SHIPPING,
            data: {
              ...values,
              order_id: orderDetail.id,
              shipping_date: dayjs(values.shipping_date).format("YYYY-MM-DD"),
            },
          })
            .then(() => {
              messageAction("saveSuccess");
              onFetchData();
              resolve();
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          setLoading(false);
          messageAction("saveFail");
        });
      },
    });
  };
  return (
    <div>
      <HeaderSubPage
        header="รายละเอียดคำสั่งซื้อ"
        subHeader="สามารถรายละเอียดคำสั่งซื้อ"
      />
      <div className="App-paper-page">
        <Row justify="start" gutter={[16, 16]}>
          <Col xl={15}>
            {loading ? (
              <Spin
                spinning={loading}
                indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
              >
                <DivRoot>
                  <Skeleton active paragraph={{ rows: 17 }} />
                </DivRoot>
              </Spin>
            ) : (
              <OrderListItem propsOrder={orderDetail} />
            )}
          </Col>
          <Col xl={9}>
            <Row gutter={[16, 16]}>
              <Col xl={24}>
                {loading ? (
                  <DivRoot>
                    <Skeleton active avatar />
                  </DivRoot>
                ) : (
                  <OrderPayment
                    propsOrder={orderDetail}
                    onApprovePayment={onApprovePayment}
                  />
                )}
              </Col>
              <Col xl={24}>
                {loading ? (
                  <DivRoot>
                    <Skeleton active avatar paragraph={{ rows: 5 }} />
                  </DivRoot>
                ) : (
                  <OrderShipment
                    propsOrder={orderDetail}
                    onConfirmShipping={onConfirmShipping}
                  />
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
}
