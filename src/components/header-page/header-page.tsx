import React from "react";
import { Typography, Tag, Modal, message } from "antd";
import styled from "styled-components";
import { PoweroffOutlined } from "@ant-design/icons";
import { propsConfirmLogout } from "src/utils/props-component";
const { Title, Text } = Typography;
const { confirm } = Modal;
interface IPropsHeader {
  header: string;
  subHeader: string;
}
export default function HeaderSubPage({ header, subHeader }: IPropsHeader) {
  const onLogout = () => {
    confirm({
      ...propsConfirmLogout,
      onOk() {
        return new Promise((resolve: any) => {
          setTimeout(() => {
            localStorage.clear();
            message
              .success("ออกจากระบบสำเร็จ..", 0.5)
              .then(() => window.location.assign("/"));
            resolve();
          }, 500);
        });
      },
    });
  };
  return (
    <DivRoot>
      <div>
        <Title style={{ marginBottom: "0px" }} level={5}>
          {header}
        </Title>
        <Text>{subHeader}</Text>
      </div>
      <div>
        <TagButton icon={<PoweroffOutlined />} color="red" onClick={onLogout}>
          ออกจากระบบ
        </TagButton>
      </div>
    </DivRoot>
  );
}

const DivRoot = styled.div`
  padding-top: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const TagButton = styled(Tag)`
  cursor: pointer;
  padding: 3px 6px;
  border-radius: 20px !important;
`;
