import React from "react";
import { Typography } from "antd";
import styled from "styled-components";
import IconForm from "src/assets/icons/icon-form.svg";
import IconImages from "src/assets/icons/icon-upload.svg";
import IconUpload from "src/assets/icons/icon-uploaded.svg";
import IconProductSet from "src/assets/icons/icon-box.svg";
const { Text } = Typography;

interface IPropsTitle {
  type: "form" | "upload" | "images" | "productSet" | 'permission';
}
const DivStyle = styled.div`
  margin-top: 1rem;
  margin-bottom: 0.5rem;
`;
export default function TitleForm({ type }: IPropsTitle) {
  return (
    <DivStyle>
      <img
        alt="icon-form"
        src={
          type === "form"
            ? IconForm
            : type === "images"
            ? IconImages
            : type === "productSet"
            ? IconProductSet
            : IconUpload
        }
      />
      <Text style={{ fontSize: "14px", color: "#90959E", marginLeft: "5px" }}>
        {type === "form"
          ? "รายละเอียดสินค้า"
          : type === "images"
          ? "รูปภาพสินค้า"
          : type === "productSet"
          ? "รายการสินค้าในเซ็ตพร้อมสูบ"
          : type === 'permission'? 'สิทธิ์การเข้าใช้งาน'
          : "รูปภาพสินค้าที่อัปโหลด"}
      </Text>
    </DivStyle>
  );
}
