import React, { useEffect, useState } from "react";
import { HeaderPage, BoxStyled, ButtonCustom } from "src/components";
import { ColumnsManageBoxes } from "src/columns";
import { useParams, useNavigate } from "react-router-dom";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { messageAction } from "src/utils/message";
import { ITableProductMain } from "src/interfaces/interface-response";
import { Button, Switch, Modal, Table, Card } from "antd";
import { IProductLists, IRowsProduct } from "src/interfaces/interface-table";
import { propsModalDelete } from "src/utils/props-component";
import { Row, Col, Input, Typography, Select } from "antd";
import { LoadingOutlined, SearchOutlined } from "@ant-design/icons";
import styled from "styled-components";
import IconBoxSet from "src/assets/icons/box-set.svg";
import IconBoxSingle from "src/assets/icons/box-single.svg";
import IconBoxFreebase from "src/assets/icons/icon-freebase.svg";
import IconBoxSaltnic from "src/assets/icons/icon-saltnic.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons";
import { SelectStyled } from "src/styles/select-styled";
import { USE_HOOK_CATEGORY, USE_HOOK_SUB_CATEGORY } from "src/hooks";
import { ISubCategory } from "src/interfaces/interface-response";
const { confirm } = Modal;

const { Text } = Typography;
const { Option } = Select;
interface IButtonAdd {
  name: string;
  key: string;
  src?: any;
  path: string;
}
interface IParams {
  name: string;
  category_id: number | undefined;
  limit: number;
  page: number;
}
const defaultParams = {
  name: "",
  category_id: undefined,
  limit: 15,
  page: 1,
};
export default function PagePodManagement() {
  const { id, name } = useParams();
  const navigate = useNavigate();
  const navigator = useNavigate();
  const [loading, setLoading] = useState(false);
  const [selectionCat, setSelectionCat] = useState<ISubCategory[]>([]);
  const [dataRows, setDataRows] = useState<ITableProductMain>({
    total_rows: 0,
    rows: [],
  });
  const [params, setParams] = useState<IParams>({
    name: "",
    category_id: Number(id),
    limit: 15,
    page: 1,
  });
  const onFetchData = (arg: IParams) => {
    setLoading(true);
    const { category_id, limit, page, name } = arg;
    if (id) {
      const URL_API = id === "set" ? API.PRODUCTS_SET : API.PRODUCTS;
      AXIOS({
        method: "get",
        url: URL_API,
        params: {
          category_id: category_id
            ? category_id
            : id === "set"
            ? null
            : Number(id),
          limit,
          page,
          name: id !== "set" ? name : null,
          search: id !== "set" ? null : name,
        },
      })
        .then(({ data }: AxiosResponse<any, any>) => {
          const { rows, total_rows }: ITableProductMain = data.data;
          setDataRows({ total_rows, rows });
          setLoading(false);
        })

        .catch((error: AxiosError<any, any>) => {
          setDataRows({
            total_rows: 0,
            rows: [],
          });
          setLoading(false);
        });
    }
  };

  const onRefreshData = () => {
    setParams({
      ...defaultParams,
    });
    onFetchData(defaultParams);
  };
  useEffect(onRefreshData, [id]);

  const onEditItem = (item: IRowsProduct) => {
    const isProductSingle = [1, 2, 3, 6];
    if (id === "set") {
      navigator(`/admin/set-product/product-set-id/${item.id}`);
    } else if (isProductSingle.some((key: number) => key === Number(id))) {
      return navigator(
        `/admin/single-product/product-id/${item.id}/parent/${id}/category/${item.category_id}/${item.name}`
      );
    } else if (Number(id) === 4) {
      // freebase
      return navigator(
        `/admin/freebase-product/product-id/${item.id}/parent/${id}/category/${item.category_id}`
      );
    } else if (Number(id) === 5) {
      // saltnic
      return navigator(
        `/admin/saltnic-product/product-id/${item.id}/parent/${id}/category/${item.category_id}`
      );
    } else {
      return navigator(
        `/admin/regular-product/product-id/${item.id}/parent/${id}/category/${item.category_id}/${item.name}`
      );
    }
  };
  const onUpdateBestSeller = (item: IRowsProduct) => {
    setLoading(true);
    AXIOS({
      method: "put",
      url: API.BEST_SELLER,
      params: {
        product_id: item.id,
      },
    })
      .then(() => {
        onRefreshData();
      })

      .catch((error: AxiosError<any, any>) => {
        messageAction("deleteFail", error.message);
        setLoading(false);
      });
  };

  const onDeleteItem = (rowData: IRowsProduct) => {
    const URL_API = id === "set" ? API.PRODUCTS_SET : API.PRODUCTS;
    const key = id === "set" ? "set_id" : "product_id";
    confirm({
      ...propsModalDelete,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "delete",
            url: URL_API,
            params: { [key]: rowData.id },
          })
            .then(() => {
              onRefreshData();
              resolve();
              messageAction("deleteSuccess");
            })
            .catch(() => {
              reject();
            });
        }).catch(() => {
          messageAction("deleteFail");
        });
      },
    });
  };
  const onDeleteItemList = (productListId: number) => {
    confirm({
      ...propsModalDelete,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "post",
            url: API.PRODUCT_LIST_ITEM,
            data: { product_list_id: [productListId] },
          })
            .then(() => {
              onRefreshData();
              resolve();
              messageAction("deleteSuccess");
            })
            .catch(() => {
              reject();
            });
        }).catch(() => {
          messageAction("deleteFail");
        });
      },
    });
  };
  const checkIsHiddenRegilar = ["1", "2", "3", "4", "5", "6", "set"];
  const checkIsHiddenSpecial = ["1", "2", "3", "6"];
  const buttonsAction: IButtonAdd[] = [
    {
      key: "single",
      name: "สินค้าเดี่ยว",
      src: IconBoxSingle,
      path: `/admin/single-product/${id}/${name}`,
    },
    {
      key: "set",
      name: "สินค้าแบบเซ็ต",
      src: IconBoxSet,
      path: `/admin/set-product`,
    },
    {
      key: "freebase",
      name: "น้ำยา Freebase",
      src: IconBoxFreebase,
      path: `/admin/freebase-product/${id}`,
    },
    {
      key: "saltnic",
      name: "น้ำยา Saltnic",
      src: IconBoxSaltnic,
      path: `/admin/saltnic-product/${id}`,
    },
    {
      key: "regular",
      name: "สินค้าแบบปกติ",
      src: IconBoxSaltnic,
      path: `/admin/regular-product/${id}/${name}`,
    },
  ];

  const checkHidden = (key: string) => {
    switch (key) {
      case "single":
        return (
          !checkIsHiddenSpecial.some((item: string) => item === id) ||
          id === "set"
        );
      case "set":
        return id !== "set";
      case "freebase":
        return id === "4" ? false : true;
      case "saltnic":
        return id === "5" ? false : true;
      case "regular":
        return checkIsHiddenRegilar.some((item: string) => item === id);
      default:
        break;
    }
  };
  useEffect(() => {
    // declare the data fetching function
    const onPrepareForm = async () => {
      if (id === "เซ็ต") {
        const responseCategory = await USE_HOOK_CATEGORY();
        setSelectionCat(responseCategory);
      } else {
        const responseCategory = await USE_HOOK_SUB_CATEGORY(id ?? "");
        setSelectionCat(responseCategory);
      }
    };

    onPrepareForm();
  }, [id]);

  const onChange = (dataField: string, value: number | string) => {
    setParams({
      ...params,
      [dataField]: value,
    });
  };
  const expandedRowRender = (data: any) => {
    return (
      <Row gutter={[8, 8]} align="middle">
        {data.product_lists.map((item: IProductLists) => (
          <Col key={item.id} xl={6}>
            <CardStyled>
              <Row gutter={[8, 4]} align="middle">
                <ColName xl={20}>
                  <div>
                    <TextBrand>ชื่อสีสินค้า</TextBrand>
                    <SpanTitle>{`สี ${item.color}`}</SpanTitle>
                  </div>
                </ColName>
                <Col xl={4}>
                  <ButtonCustom
                    shape="circle"
                    type="dashed"
                    icon="delete"
                    onClick={() => onDeleteItemList(item.id)}
                  />
                </Col>
              </Row>
            </CardStyled>
          </Col>
        ))}
      </Row>
    );
  };

  const columns: any = [
    ...ColumnsManageBoxes,
    {
      title: "สินค้าขายดี",
      key: "is_best_seller",
      dataIndex: "is_best_seller",
      width: 100,
      align: "center",
      render: (value: boolean, item: IRowsProduct) => (
        <Switch
          defaultChecked={value}
          onChange={() => onUpdateBestSeller(item)}
        />
      ),
    },
    {
      title: "แก้ไข",
      key: "action",
      align: "center",
      width: 80,
      fixed: "right",
      render: (_: any, item: any) => (
        <ButtonCustom
          onClick={() => onEditItem(item)}
          size="small"
          type="text"
          text="แก้ไขสินค้า"
        />
      ),
    },
    {
      title: "ลบ",
      key: "action",
      align: "center",
      fixed: "right",
      width: 90,
      render: (_: any, item: IRowsProduct) => (
        <Button
          onClick={() => onDeleteItem(item)}
          danger
          style={{ fontSize: 12, fontWeight: 500 }}
          type="text"
        >
          ลบสินค้า
        </Button>
      ),
    },
  ];

  return (
    <div>
      <HeaderPage
        header={`จัดการรายการสินค้า : ประเภท${name}`}
        subHeader="สามารถเพิ่มรายการประเภทสินค้า เพิ่มได้ / ลบ / เพิ่มสินค้า"
      />
      <div className="App-paper-page">
        <BoxStyled color="grey" type="table">
          <BoxRoot>
            <Row gutter={[12, 8]}>
              <Col xl={18} xs={24}>
                <Row gutter={[4, 12]} align="bottom">
                  <Col xl={24}>
                    <Text strong>{"ค้นหาจากสินค้าทั้งหมด"}</Text>
                    <Text style={{ marginLeft: "5px" }} type="secondary">
                      {`${dataRows.total_rows}`}
                    </Text>
                  </Col>
                  <Col xl={24} xs={24}>
                    <BoxActionFilterTableBox>
                      <Row gutter={[4, 4]}>
                        <Col xl={9} xs={24}>
                          <Input
                            allowClear
                            placeholder="ค้นหาชื่อสินค้า"
                            suffix={<SearchOutlined />}
                            style={{ width: "100%" }}
                            value={params.name}
                            onChange={(e: any) => {
                              const value = e.target.value;
                              if (value) {
                                onChange("name", value);
                              } else {
                                onFetchData({ ...params, name: value });
                                setParams({ ...params, name: value });
                              }
                            }}
                          />
                        </Col>
                        <Col xl={9} xs={24}>
                          <SelectStyled
                            allowClear
                            showSearch
                            placeholder="เลือกตามประเภท"
                            optionFilterProp="children"
                            onChange={(value: any) => {
                              if (value) {
                                onChange("category_id", value);
                              } else {
                                onFetchData({ ...params, category_id: value });
                                setParams({ ...params, category_id: value });
                              }
                            }}
                            value={params.category_id}
                            filterOption={(input, option) =>
                              (option!.children as unknown as string)
                                .toLowerCase()
                                .includes(input.toLowerCase())
                            }
                          >
                            {selectionCat.map((item: ISubCategory) => (
                              <Option key={item.id} value={item.id}>
                                {item.name}
                              </Option>
                            ))}
                          </SelectStyled>
                        </Col>
                        <Col xl={6} xs={24}>
                          <ButtonCustom
                            width="100%"
                            type="primary"
                            icon="search"
                            text="ค้นหารายการสินค้า"
                            onClick={() => onFetchData(params)}
                          />
                        </Col>
                      </Row>
                    </BoxActionFilterTableBox>
                  </Col>
                </Row>
              </Col>
              <Col xl={6} xs={24}>
                <BoxActionButtonTableBox>
                  <Row gutter={[8, 12]}>
                    <Col xl={24} style={{ textAlign: "end" }}>
                      <Text style={{ marginLeft: "5px" }} strong>
                        รูปแบบสินค้า
                      </Text>
                      <Text style={{ marginLeft: "5px" }} type="secondary">
                        {"(เลือกเพิ่มสินค้าตามรูปแบบสินค้า)"}
                      </Text>
                    </Col>
                    {buttonsAction.map((item: IButtonAdd) => (
                      <Col
                        hidden={checkHidden(item.key)}
                        key={item.key}
                        xl={24}
                        onClick={() => navigate(item.path)}
                      >
                        <RowStyle align="middle" justify="space-between">
                          <ColIcon xl={4}>
                            <img alt="icon" src={item.src} />
                          </ColIcon>
                          <Col xl={17} style={{ textAlign: "center" }}>
                            <SpanLabel style={{ fontWeight: 500 }}>
                              {`เพิ่ม${name}`}
                            </SpanLabel>
                          </Col>
                          <Col xl={3}>
                            <FontAwesomeIcon
                              style={{ color: "#4D5FAC" }}
                              fontSize={18}
                              icon={faCirclePlus}
                            />
                          </Col>
                        </RowStyle>
                      </Col>
                    ))}
                  </Row>
                </BoxActionButtonTableBox>
              </Col>
            </Row>
          </BoxRoot>

          <TableStyled
            rowKey="id"
            pagination={{
              total: dataRows.total_rows,
              pageSize: params.limit,
              current: params.page,
              onChange: (page: number) => {
                onFetchData({ ...params, page });
                setParams({ ...params, page });
              },
            }}
            dataSource={dataRows.rows}
            loading={{
              tip: "กำลังโหลดรายการสินค้า",
              spinning: loading,
              indicator: <LoadingOutlined style={{ fontSize: 24 }} spin />,
            }}
            expandable={{
              columnWidth: 20,
              expandedRowRender,
              rowExpandable: (record: any) => {
                return record.product_lists === undefined ||
                  record.product_lists.length === 1
                  ? false
                  : true;
              },
            }}
            sticky
            size="small"
            columns={columns}
          ></TableStyled>
        </BoxStyled>
      </div>
    </div>
  );
}
const TableStyled = styled(Table)`
  padding: 0px 10px 0px 10px;
`;
const ColName = styled(Col)`
  display: flex;
  align-items: center;
`;
const SpanTitle = styled.span`
  font-size: 12px;
  font-weight: 500;
  margin-right: 1rem;
  color: #4d556b;
`;
const TextBrand = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  color: grey;
`;
const CardStyled = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 0px 7px 0px 7px;
  background: white;
  & .ant-card-body {
    padding: 10px;
  }
`;

const BoxRoot = styled.div`
  margin: 1.5rem 2rem 2rem 2rem;
`;
const RowStyle = styled(Row)`
  border-radius: 5px;
  background: #f8f9fe;
  height: 4rem;
  cursor: pointer;
  &:hover {
    box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  }
`;
const ColIcon = styled(Col)`
  padding: 10px;
  height: 100%;
  display: flex;
  align-items: center;
  border-radius: 5px 0px 0px 5px;
`;
const SpanLabel = styled.span`
  color: #5c68ff;
  font-size: 14px;
`;
const BoxActionFilterTableBox = styled.div`
  padding: 15px 15px 15px 15px;
  border-radius: 5px;
  background: #f9f9f9;
`;
const BoxActionButtonTableBox = styled.div`
  border-radius: 5px;
  background: #ffff;
`;
