import React, { useEffect, useState } from "react";
import { Col, Form, Input, Row, Select, Spin } from "antd";
import { ButtonCustom } from "src/components";
import { ModalFormStyled } from "src/styles/modal-styled";
import {
  IPermissionUser,
  IPropsModalUser,
} from "src/interfaces/interface-form";
import { API, AXIOS } from "src/services";
import { messageAction } from "src/utils/message";
import { AxiosError } from "axios";
import { userPermission } from "src/data";

const { Option } = Select;

interface iActions {
  menu_name: string;
  add: boolean;
  view: boolean;
  edit: boolean;
  delete: boolean;
}
interface IProps {
  modalProps: IPropsModalUser;
  onClose: () => void;
  onFinished: () => void;
}
export default function ModalFormPermission({
  modalProps,
  onClose,
  onFinished,
}: IProps) {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const { visible, type, defaultData } = modalProps;

  useEffect(() => {
    if (visible) {
      form.setFieldsValue({
        ...defaultData,
      });
    }
  }, [visible]);

  const onSubmitData = () => {
    setLoading(true);
    const data = form.getFieldsValue();
    const values = { ...data, id: type === "add" ? null : defaultData.id };
    AXIOS({
      method: type === "add" ? "post" : "put",
      url: API.USERS,
      data: values,
    })
      .then(() => {
        messageAction("saveSuccess");
        setLoading(false);

        form.resetFields();
        onFinished();
      })
      .catch((error: AxiosError<any, any>) => {
        messageAction("saveFail");
        setLoading(false);
      });
  };
  return (
    <div>
      <ModalFormStyled
        width={600}
        title={`การจำกัดสิทธิ์การใช้งาน : ${defaultData.name}`}
        visible={visible}
        onCancel={onClose}
        footer={
          <ButtonCustom
            onClick={onSubmitData}
            type="primary"
            icon="save"
            text="บันทึกการเปลี่ยนแปลง"
          />
        }
      >
        <Spin spinning={loading} tip="กำลังบันทึก....">
          <Form layout="vertical" form={form}>
            <Row gutter={[16, 4]}>
              <Col xl={12} sm={12} xs={24}>
                <Form.Item
                  name="username"
                  required
                  label="ชื่อผู้ใช้งาน (username)"
                >
                  <Input size="large" />
                </Form.Item>
              </Col>
              <Col xl={12} sm={12} xs={24}>
                <Form.Item
                  name="password"
                  required
                  label="Password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input.Password size="large" />
                </Form.Item>
              </Col>
              <Col xl={12} sm={12} xs={24}>
                <Form.Item name="name" required label="ชื่อ">
                  <Input size="large" />
                </Form.Item>
              </Col>
              <Col xl={12} sm={12} xs={24}>
                <Form.Item
                  required
                  label="สิทธิ์การเข้าใช้งาน"
                  name="user_type_id"
                >
                  <Select
                    showSearch
                    placeholder="สิทธิ์การเข้าใช้งาน"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option!.children as unknown as string)
                        .toLowerCase()
                        .includes(input.toLowerCase())
                    }
                  >
                    {userPermission.map((item: IPermissionUser) => (
                      <Option value={item.id}>{item.type_name}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col xl={12} sm={12} xs={24}>
                <Form.Item
                  name="phone"
                  label="เบอร์โทรศัพท์"
                  rules={[
                    {
                      required: true,
                      message: "กรุณาระบุหมายเลขโทรศัพท์!",
                    },
                  ]}
                >
                  <Input style={{ width: "100%" }} maxLength={10} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Spin>
      </ModalFormStyled>
    </div>
  );
}
