import { ISubMenu } from "src/interfaces/interface-side-navbar";

const MockCatProduct: any[] = [
  {
    title: "กล่อง",
    key: "box",
    path: "boxes",
  },
  {
    title: "พอต",
    key: "pod",
    path: "pod",
  },
  {
    title: "มอท",
    key: "mod",
    path: "mod",
  },
  {
    title: "น้ำยา Freebase",
    key: "freebase",
    path: "freebase",
  },
  {
    title: "น้ำยา Saltnic",
    key: "saltnic",
    path: "saltnic",
  },
  {
    title: "น้ำยาทางร้าน",
    key: "store-liquid",
    path: "store-liquid",
  },
  {
    title: "อะตอม",
    key: "atom",
    path: "atom",
  },
  {
    title: "คอย",
    key: "coil",
    path: "coil",
  },
  {
    title: "สำลี ลวด",
    key: "cotton-wire",
    path: "cotton-wire",
  },
  {
    title: "ถ่าน รางชาร์ท",
    key: "battery-charger",
    path: "battery-charger",
  },
  {
    title: "อุปกรณ์ต่างๆ",
    key: "accessory",
    path: "accessories",
  },
];

export default MockCatProduct;
