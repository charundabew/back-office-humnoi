import React from "react";
import styled from "styled-components";
import { Tag } from "antd";

interface IPropsTagStatus {
  status: "active" | "inactive";
}
const TagStyled = styled(Tag)`
  padding: 0px 8px;
  border-radius: 15px;
  font-size: 12px;
`;
export default function TagStatus({ status }: IPropsTagStatus) {
  return (
    <TagStyled color={status === "active" ? "geekblue" : "red"}>
      {status === "active" ? "เปิดใช้งาน" : "ปิดใช้งาน"}
    </TagStyled>
  );
}
