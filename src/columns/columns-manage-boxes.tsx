import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { TagStatus } from "src/components";
import { Avatar, Switch } from "antd";
import { renderImage } from "src/utils/helper";
import { convertToPrice, patternValueCount } from "src/utils/pattern-number";

const ColumnsManageBoxes: ColumnsType<any> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 50,
    // fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "รูปสินค้า",
    dataIndex: "image_path",
    key: "image_path",
    align: "center",
    width: 70,
    // fixed: true,
    render: (value, item) => (
      <Avatar
        shape="square"
        size={48}
        src={renderImage(value ?? item.image_file)}
      />
    ),
  },
  {
    title: "ชื่อสินค้า",
    dataIndex: "name",
    key: "name",
    width: 120,
  },
  {
    title: "ประเภท",
    dataIndex: "category_name",
    key: "category_name",
    align: "left",
    width: 90,
  },
  {
    title: "ราคา/ชิ้น",
    dataIndex: "price_show",
    key: "price_show",
    align: "right",
    width: 60,
    render: (value: number) => <span>{convertToPrice(value)}</span>,
  },
  {
    title: "สถานะ",
    key: "is_active",
    dataIndex: "is_active",
    width: 100,
    align: "center",
    render: (value) => <TagStatus status={value ? "active" : "inactive"} />,
  },
];

export default ColumnsManageBoxes;
