import React, { useEffect, useState, useContext } from "react";
import {
  Checkbox,
  Col,
  Drawer,
  Empty,
  Form,
  Input,
  Pagination,
  Row,
  Skeleton,
  Spin,
  Tooltip,
  Typography,
} from "antd";
import { ButtonCustom, TagStatus } from "src/components";
import styled from "styled-components";
import ImageProductSet from "src/assets/images/img-add-product-set.svg";
import { ITableProduct } from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import { AxiosResponse } from "axios";
import { IRowsProduct } from "src/interfaces/interface-table";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { IPropsDrawerSelected } from "src/interfaces/interface-form";
import { renderImage } from "src/utils/helper";

const { Text, Title } = Typography;

interface IPropsModalCaergory {
  onClose: () => void;
  propsDrawer: IPropsDrawerSelected;
}
export default function DrawerSelectedProduct({
  onClose,
  propsDrawer,
}: IPropsModalCaergory) {
  const { isOpen, category, productType } = propsDrawer;
  const [form] = Form.useForm();
  const pickOnlyOneItem = [1, 2, 3, 7, 8];
  const { selectedProduct, onSetToStore } = useContext(GlobalContext);
  const [loading, setLoading] = useState(true);
  const [itemInCart, setItemInCart] = useState<IRowsProduct[]>([]);
  const [dataRows, setDataRows] = useState<ITableProduct>({
    total_rows: 0,
    rows: [],
    limit: 10,
    page: 1,
  });
  const onGetProduct = (name: string) => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.PORDUCTNONSET,
      params: {
        category_id: category.id,
        limit: 10,
        page: dataRows.page,
        name,
      },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: ITableProduct = data.data;
        setDataRows(response);
        setLoading(false);
      })

      .catch(() => {
        setDataRows({
          total_rows: 0,
          rows: [],
          limit: 10,
          page: dataRows.page,
        });
        setLoading(false);
      });
  };
  useEffect(() => {
    if (isOpen) {
      setDataRows({
        total_rows: 0,
        rows: [],
        limit: 10,
        page: 1,
      });
    }
    form.resetFields();
  }, [isOpen]);
  useEffect(() => {
    if(isOpen){
      return onGetProduct("");
    }
  }, [dataRows.page,category.id]);
  const checkboxItem = (e: any, row: IRowsProduct) => {
    const { checked } = e.target;
    const tmp = itemInCart;
    const mainId = [0,1, 2, 3];
    switch (checked) {
      case true:
        if (productType === "main") {
          const findExistMain = tmp.findIndex((item: IRowsProduct) =>
            mainId.some(
              (main: number) =>
                main === item.root_parent_id
            )
          );
          if (findExistMain > -1) {
            tmp[findExistMain] = { ...row, freebies: false };
            setItemInCart([...tmp]);
          } else {
            tmp.unshift({ ...row, freebies: false });
            setItemInCart([...tmp]);
          }
        } else if (category.id === 7 || category.id === 8) {
          //coil and cotton can pick only one item
          const findExist = tmp.findIndex(
            (item: IRowsProduct) => item.category_id === category.id
          );
          if (findExist > -1) {
            tmp[findExist] = { ...row, freebies: false };
            setItemInCart([...tmp]);
          } else {
            setItemInCart([...itemInCart, { ...row, freebies: false }]);
          }
        } else {
          const addItem: IRowsProduct[] = [
            ...itemInCart,
            { ...row, freebies: false },
          ];
          setItemInCart(addItem);
        }

        break;
      case false:
        const removeItem: IRowsProduct[] = tmp.filter(
          (item: IRowsProduct) => item.id !== row.id
        );
        setItemInCart(removeItem);
        break;
      default:
        break;
    }
  };
  const onCancel = () => {
    setItemInCart([]);
    onClose();
  };
  const onSubmitInCart = () => {
    onSetToStore({
      name: "selectedProduct",
      value: itemInCart,
    });
    onClose();
    setItemInCart([]);
  };
  useEffect(() => {
    setItemInCart(selectedProduct);
  }, [selectedProduct]);
  const onPainationChange = (page: number) => {
    setDataRows({
      ...dataRows,
      page,
    });
  };
  return (
    <div>
      <Drawer
        width={600}
        placement="right"
        closable={false}
        onClose={onClose}
        visible={isOpen}
        title="เลือกรายการสินค้าในเซ็ต"
        footer={
          <Row gutter={[8, 8]} justify="space-between" align="middle">
            <Col span={7}>
              <ButtonCustom
                onClick={onCancel}
                type="ghost"
                icon="cancel"
                text="ยกเลิกข้อมูล"
              />
            </Col>
            <Col span={6}>
              <ButtonCustom
                type="primary"
                icon="cart-add"
                text="เพิ่มสินค้าในเซ็ต"
                onClick={onSubmitInCart}
              />
            </Col>
          </Row>
        }
      >
        <DivHeader>
          <ImageIcon src={ImageProductSet} alt="icon-product" />
          <Title level={5}>{`สินค้าประเภท${category.name}ทั้งหมด`}</Title>
          {pickOnlyOneItem.some((idPick: number) => idPick === category.id) ? (
            <Text type="danger" style={{ fontSize: "14px" }}>
              เลือกรายการสินค้าได้แค่ 1 รายการ *
            </Text>
          ) : (
            <Text type="secondary" style={{ fontSize: "14px" }}>
              สามารถเลือกรายการสินค้าได้มากกว่า 1 รายการ
            </Text>
          )}
        </DivHeader>
        <BoxSection>
          <Form form={form}>
            <Form.Item name="search">
              <Input.Search
                allowClear
                onSearch={(value: string) => {
                  onGetProduct(value);
                }}
                placeholder="ค้นหาชื่อสินค้า"
                size="large"
                style={{ width: "100%" }}
              />
            </Form.Item>
          </Form>
        </BoxSection>
        <Spin spinning={loading}>
          <Skeleton style={{ marginTop: "1rem" }} loading={loading} />
        </Spin>
        <BoxSectionItem>
          <Row gutter={[16, 8]} align="middle" hidden={loading}>
            {dataRows.rows.length === 0 ? (
              <Col xs={24}>
                <Empty description="ไม่พบรายการสินค้า" />
              </Col>
            ) : (
              dataRows.rows.map((product: IRowsProduct) => (
                <Col xl={12} md={12} key={product.id}>
                  <DivProduct>
                    <Row
                      gutter={[16, 8]}
                      align="middle"
                      justify="space-between"
                    >
                      <Col xs={24}>
                        <Tooltip title={product.name}>
                          <TextName>{product.name}</TextName>
                        </Tooltip>
                      </Col>
                      <Col xl={3}>
                        <Checkbox
                          checked={itemInCart.some(
                            (item: IRowsProduct) => item.id === product.id
                          )}
                          onChange={(e: any) => checkboxItem(e, product)}
                        />
                      </Col>
                      <Col xl={7} md={7}>
                        <ImageProduct
                          src={renderImage(product.image_path)}
                          alt="product"
                        />
                      </Col>
                      <Col xl={14}>
                        <TextCaption>{`ราคาสินค้า ${product.price_show} บาท`}</TextCaption>
                        <TagStatus
                          status={product.is_active ? "active" : "inactive"}
                        />
                      </Col>
                    </Row>
                  </DivProduct>
                </Col>
              ))
            )}
          </Row>
        </BoxSectionItem>
        <BoxSection style={{ position: "absolute", bottom: "70px" }}>
          <Pagination
            defaultCurrent={dataRows.page}
            total={dataRows.total_rows}
            current={dataRows.page}
            pageSize={dataRows.limit}
            onChange={onPainationChange}
          />
        </BoxSection>
      </Drawer>
    </div>
  );
}
const ImageProduct = styled.img`
  width: 100%;
  height: auto;
  border-radius: 5px;
`;
const TextCaption = styled.p`
  margin-bottom: 2px;
  margin-top: 2px;
  font-size: 12px;
`;

const TextName = styled.p`
  margin-bottom: 1px;
  margin-top: 1px;
  font-size: 12px;
  color: #363e5c;
  font-weight: 500;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
const DivProduct = styled.div`
  background: #f9fafb;
  border-radius: 5px;
  padding: 10px;
`;

const DivHeader = styled.div`
  text-align: center;
`;
const BoxSection = styled.div`
  margin-top: 1rem;
`;

const BoxSectionItem = styled.div`
  min-height: 20vh;
  margin-top: 1rem;
  border: 1px solid #f9fafb;
`;
const ImageIcon = styled.img`
  width: 3rem;
  height: 3rem;
  display: block;
  margin: auto;
  margin-bottom: 1rem;
`;
