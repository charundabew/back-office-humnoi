import React, { useState, useEffect } from "react";
import { Avatar, Col, Layout, Menu, Row, Typography } from "antd";
import {
  DivTitle,
  ContentStyled,
  MenuItem,
  DivHeader,
} from "./side-navebar-styled";
import { Link, Outlet, useLocation } from "react-router-dom";
import { sideMenuItem } from "src/data";
import { IMenuItem, ISubMenu } from "src/interfaces/interface-side-navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {} from "@fortawesome/free-regular-svg-icons";
import { UserOutlined } from "@ant-design/icons";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";

const { Sider } = Layout;
const { SubMenu } = Menu;
const { Text } = Typography;


export default function SideNavbar() {
  const location = useLocation();
  const [statemenuItem, setStateMenuitem] = useState<IMenuItem[]>([]);
  const [selctedKey, setSelectedKey] = useState("order");
  const { pathname } = location;
  const permission = localStorage.getItem("name") ?? "";
  const username = localStorage.getItem("username") ?? "";

  const onSelctedKey = () => {
    const flat = statemenuItem
      .map((item: IMenuItem) => item.submenu ?? item)
      .flat(1);
    const findPathname = flat.find((item: any) => {
      return item.path === decodeURI(pathname);
    });
    setSelectedKey(findPathname?.key ?? "");
  };
  const getCatergories = () => {
    AXIOS({
      method: "get",
      url: API.CATEGORY,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response = data.data;
        const addPath = response.map((item: ISubMenu) => {
          const obj = {
            ...item,
            key: String(item.id),
            path: `/admin/product/${item.name}/${item.id}`,
          };
          return obj;
        });
        const updateData: IMenuItem[] = sideMenuItem.map((item: IMenuItem) => {
          item.submenu =
            item.key === "product"
              ? [
                  {
                    name: "เซ็ตพร้อมสูบ",
                    id: "set",
                    key: "set",
                    is_active: true,
                    path: "/admin/product/เซ็ตพร้อมสูบ/set",
                  },
                  ...addPath,
                ]
              : item.submenu;
          return item;
        });
        setStateMenuitem(updateData);
      })
      .catch((error: AxiosError<any, any>) => {});
  };
  useEffect(getCatergories, []);
  useEffect(onSelctedKey, [pathname, statemenuItem]);
  const checkHiddenMenu = (key: string) => {
    if (key === "report" || key === "permission") {
      if (permission === "owner") {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };
  return (
    <div>
      <Layout>
        <Sider
          width={220}
          theme="light"
          trigger={null}
          style={{
            overflow: "auto",
            height: "100vh",
            position: "fixed",
            left: 0,
            top: 0,
            bottom: 0,
            zIndex: 2,
          }}
        >
          <DivHeader>
            <DivTitle>
              <div style={{ display: "grid" }}>
                <span
                  style={{ color: "white", fontSize: "14px", fontWeight: 500 }}
                >
                  ระบบจัดการร้านค้า
                </span>
              </div>
            </DivTitle>
            <DivTitle
              style={{
                background: "#eff2f5",
                border: "0.5px solid hsl(213deg 85% 97%)",
                marginTop: "8px",
                borderRadius: "4px",
                padding: "2px 10px",
              }}
            >
              <Avatar
                size="small"
                style={{ backgroundColor: "#5c68ff" }}
                icon={<UserOutlined />}
              />

              <div style={{ display: "grid", marginLeft: "10px" }}>
                <Text style={{ color: "#5151f9" }} strong>
                  {username}
                </Text>
                <Text style={{ color: "#5151f9" }} type="secondary">
                  {permission === 'admin' ?'เจ้าหน้าที่ดูแลระบบ (Admin)' : 'เจ้าของกิจการ (Owner)'}
                </Text>
              </div>
            </DivTitle>
          </DivHeader>
          <Menu selectedKeys={[selctedKey]} theme="light" mode="inline">
            {statemenuItem.map((menu: IMenuItem) =>
              menu.submenu ? (
                <SubMenu
                  key={menu.key}
                  title={menu.menu}
                  icon={<FontAwesomeIcon icon={menu.icon} />}
                >
                  {menu.submenu?.map((sub: ISubMenu) => (
                    <MenuItem key={sub.key}>
                      {sub.name}
                      <Link to={`/admin/product/${sub.name}/${sub.id}`} />
                    </MenuItem>
                  ))}
                </SubMenu>
              ) : (
                <MenuItem
                  hidden={checkHiddenMenu(menu.key)}
                  icon={<FontAwesomeIcon icon={menu.icon} />}
                  key={menu.key}
                >
                  {menu.menu}
                  <Link to={`${menu.path}`} />
                </MenuItem>
              )
            )}
          </Menu>
        </Sider>
        <Layout style={{ marginLeft: 220 }}>
          <Row justify="center">
            <Col xl={23}>
              <ContentStyled style={{ overflow: "initial" }}>
                <Outlet />
              </ContentStyled>
            </Col>
          </Row>
        </Layout>
      </Layout>
    </div>
  );
}
