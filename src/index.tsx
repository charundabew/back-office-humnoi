import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "antd/dist/antd.variable.min.css";
import { ConfigProvider } from "antd";
import ReducerComposer from "src/contexts/store/context-composer";

import Themes from "./theme/ant-theme";
const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
ConfigProvider.config({ ...Themes });
root.render(
    <ConfigProvider>
      <ReducerComposer>
        <App />
      </ReducerComposer>
    </ConfigProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
