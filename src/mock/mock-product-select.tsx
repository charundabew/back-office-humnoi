import { IProductSelect } from "src/interfaces/interface-form";
import { srcBox } from "./mock-img-src";

export const MockProductSelect: IProductSelect[] = [
  {
    name: "Vandy Vape PR SE 95W Kit กล่องบีบ",
    price: 2340,
    stock: 50,
    src: srcBox,
  },
  {
    name: "Dooze salt",
    price: 2340,
    stock: 50,
    src: srcBox,
  },
  {
    name: "Brain freeze saltnic",
    price: 2340,
    stock: 50,
    src: srcBox,
  },
  {
    name: "Vandy Vape PR SE 95W Kit กล่องบีบ",
    price: 2340,
    stock: 50,
    src: srcBox,
  },
];
export default MockProductSelect;

