import React from "react";
import styled from "styled-components";
import { Tag } from "antd";

interface IPropsTag {
  text: string;
  color: "blue" | "green" | "red" | "volcano" | "gold" | "orange" | "purple";
}
const TagStyled = styled(Tag)`
  padding: 6px;
  border-radius: 5px;
`;
export default function CardAction({ text, color }: IPropsTag) {
  return <TagStyled color={color}>{text}</TagStyled>;
}
