import { DataTypeOrder } from "src/interfaces/interface-table";

const mockDataOrderTable: any[] = [
  {
    order_no: "#OOPS0082222",
    order_name: "สุริยะ อาจหาญ",
    order_date: new Date(),
    order_status: "สำเร็จ",
    shipping_date: new Date(),
    track_number: "#11000066788",
    shipping_price: 50,
    product_price: 250,
    order_total: 2000,
  },
  {
    order_no: "#OOPS0082222",
    order_name: "สุริยะ อาจหาญ",
    order_date: new Date(),
    order_status: "รอจัดส่ง",
    shipping_date: new Date(),
    track_number: "#11000066788",
    shipping_price: 50,
    product_price: 250,
    order_total: 2000,
  },
  {
    order_no: "#OOPS0082222",
    order_name: "สุริยะ อาจหาญ",
    order_date: new Date(),
    order_status: "รอตรวจสอบ",
    shipping_date: new Date(),
    track_number: "#11000066788",
    shipping_price: 50,
    product_price: 250,
    order_total: 2000,
  },
  {
    order_no: "#OOPS0082222",
    order_name: "สุริยะ อาจหาญ",
    order_date: new Date(),
    order_status: "รอชำระเงิน",
    shipping_date: new Date(),
    track_number: "#11000066788",
    shipping_price: 50,
    product_price: 250,
    order_total: 2000,
  },
];

export default mockDataOrderTable;
