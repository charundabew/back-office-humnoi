import { message, notification } from "antd";
import axios from "axios";
import HOST from "./host";

const AXIOS = axios.create({
  baseURL: HOST,
});

AXIOS.interceptors.request.use((request: any) => {
  request.headers.Authorization = `Bearer ${localStorage.getItem(
    "access_token"
  )}`;
  return request;
});

AXIOS.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response) {
      if (error.response.status === 401) {
        localStorage.clear();
        notification.warning({
          message: "กรุณาเข้าสู่ระบบใหม่",
          description:
            "หมดเวลาการใช้งานในระบบ กรุณาเข้าสู่ระบบอีกครั้งเพื่อเข้าใช้งาน",
        });
        message
          .loading("กำลังออกจากระบบ..", 0.5)
          .then(() => window.location.assign("/"));
      } else {
        return Promise.reject(error);
      }
    } else {
      console.log("connect internet");
      // InternetNote();
    }
  }
);
export default AXIOS;
