import { IMenuItem } from "src/interfaces/interface-side-navbar";
import {
  faTasks,
  faFolderTree,
  faUserCog,
  faImage,
  faCartPlus,
  faTags,
  faChartLine
} from "@fortawesome/free-solid-svg-icons";



const sideMenuItem: IMenuItem[] = [
  {
    menu: "รายการคำสั่งซื้อ",
    key: "order",
    path: "/admin",
    submenu: null,
    icon: faTasks,
  },
  {
    menu: "ตั้งค่าแบนเนอร์",
    key: "banner",
    path: "/admin/banner",
    submenu: null,
    icon: faImage,
  },
  {
    menu: "จัดการแบรนด์สินค้า",
    key: "brands",
    path: "/admin/brands",
    submenu: null,
    icon: faTags,
  },
  {
    menu: "จัดการประเภทรายการสินค้า",
    key: "category",
    path: "/admin/categories",
    submenu: null,
    icon: faFolderTree,
  },
  {
    menu: "จัดการสินค้า",
    key: "product",
    path: "/admin/product",
    icon: faCartPlus,
    submenu: [],
  },
  {
    menu: "การออกรายงาน",
    key: "report",
    path: "/admin/reports-management",
    submenu: null,
    icon: faChartLine,
  },
  {
    menu: "การจำกัดสิทธิ์การใช้งาน",
    key: "permission",
    path: "/admin/permission-management",
    submenu: null,
    icon: faUserCog,
  },
];

export default sideMenuItem;
