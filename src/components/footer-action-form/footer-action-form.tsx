import React from "react";
import { Row, Col, Form } from "antd";
import { ButtonCustom } from "src/components";

interface IPropsAction {
  onClickCancle?: () => void;
}
export default function HeaderSubPage({
  onClickCancle,
}: IPropsAction) {
  return (
    <div style={{marginTop: '1rem'}}>
      <Row justify="space-between">
        <Col xl={3}>
          <ButtonCustom width='100%' icon='cancel' onClick={onClickCancle} type='ghost' text="ยกเลิกข้อมูล" />
        </Col>
        <Col xl={3}>
          <Form.Item>
          <ButtonCustom htmlType='submit' width='100%' icon="save"  type='primary' text="บันทึกข้อมูล" />
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
}
