import { Layout, Menu } from "antd";
import styled from "styled-components";

const { Content, Header } = Layout;

export const MenuItem = styled(Menu.Item)`
  font-size: 12px;
`;
export const HeaderMenu = styled(Header)`
  background: #eff2f5;
  opacity: 90%;
  padding: 1rem;
  height: 3.5rem;
`;
export const ContentStyled = styled(Content)`
  padding: 1rem;
  height: auto;
  min-height: 100vh;
  display: block;
  margin: auto;
  width: 100%;
`;
export const DivHeader = styled.div`
  background: #5151f9;
  padding: 0.7rem;
`;
export const DivTitle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
