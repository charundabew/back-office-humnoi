import React from "react";
import styled from "styled-components";
import { Tag } from "antd";

interface IPropsTagStatus {
  status: string;
}
const renderColorBg = (color: any) => {
  switch (color) {
    case "green":
      return "#E2FFD4";
    case "blue":
      return "#CCEDF8";
    case "orange":
      return "#F9DDC4";
    case "yellow":
      return "#FBF2D2";
    default:
      break;
  }
};
const renderColorText = (color: any) => {
  switch (color) {
    case "green":
      return "#6BBF43";
    case "blue":
      return "#35A4E2";
    case "orange":
      return "#F37F14";
    case "yellow":
      return "#F3C214";
    default:
      break;
  }
};
const TagStyled = styled(Tag)`
  width: 100px;
  padding: 3px 5px;
  border-radius: 15px;
  border: unset;
  text-align: center;
  font-size: 12px;
  background-color: ${(props) => renderColorBg(props.color)};
  color: ${(props) => renderColorText(props.color)};
`;
export default function TagStatus({ status }: IPropsTagStatus) {
  const renderColor = (status: string) => {
    switch (status) {
      case "สำเร็จ":
        return "green";
      case "รอจัดส่ง":
        return "blue";
      case "รอตรวจสอบ":
        return "orange";
      case "รอชำระเงิน":
        return "yellow";
      default:
        break;
    }
  };
  return <TagStyled color={renderColor(status)}>{status}</TagStyled>;
}
