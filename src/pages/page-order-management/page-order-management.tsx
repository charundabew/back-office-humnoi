import React, { useEffect, useState } from "react";
import { Row, Col, Input, Typography, DatePicker, Select, Radio } from "antd";
import {
  HeaderPage,
  ButtonCustom,
  BoxStyled,
  TableMain,
  TitleTable,
} from "src/components";
import { FilterFilled, SearchOutlined } from "@ant-design/icons";
import { ColumnsManageOrder } from "src/columns";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { ITableProduct } from "src/interfaces/interface-response";
import { useNavigate } from "react-router-dom";
import { orderStatus } from "src/data";

const { Text } = Typography;

interface ICheckbox {
  name: string;
  value: string;
}
const checkboxes: ICheckbox[] = [
  {
    name: "รายวัน",
    value: "day",
  },
  {
    name: "รายสัปดาห์",
    value: "week",
  },
  {
    name: "รายเดือน",
    value: "month",
  },
];
interface IParams {
  page: number;
  limit: number;
  status: number | "";
  search: string;
  duration: "day" | "week" | "month" | "date" | "";
  start: string;
  end: string;
}
export default function PageOrderManagement() {
  const navigator = useNavigate();
  const [loading, setLoading] = useState(true);
  const [dataRows, setDataRows] = useState<ITableProduct>({
    total_rows: 0,
    rows: [],
    limit: 10,
    page: 1,
  });
  const [params, setParams] = useState<IParams>({
    page: 1,
    limit: 10,
    status: "",
    search: "",
    duration: "date",
    start: "",
    end: "",
  });
  const onFetchData = () => {
    setLoading(true);
    const { duration, start, end }: IParams = params;
    const isDate = duration === "date";
    const startDate = isDate ? start : "";
    const endDate = isDate ? end : "";
    const updateParams: IParams = {
      ...params,
      duration: isDate ? "" : duration,
      start: startDate,
      end: endDate,
    };
    AXIOS({
      method: "get",
      url: API.ORDERS,
      params: updateParams,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: ITableProduct = data.data;
        setDataRows(response);
        setLoading(false);
      })

      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });
  };
  useEffect(onFetchData, []);

  const onHandleChangeParams = (dataField: string, value: any) => {
    setParams({ ...params, [dataField]: value });
  };
  return (
    <div>
      <HeaderPage
        header="รายงานคำสั่งซื้อ "
        subHeader="สามารถเรียกดูสรุปรายงานคำสั่งซื้อได้"
      />
      <div className="App-paper-page">
        <BoxStyled color="white">
          <Row align="middle" gutter={[8, 16]}>
            <Col lg={12}>
              <Row gutter={[8, 8]} justify="center" align="middle">
                <Col xl={2}>
                  <Radio
                    checked={params.duration === "date"}
                    onChange={() => onHandleChangeParams("duration", "date")}
                  />
                </Col>
                <Col xl={10}>
                  <DatePicker
                    allowClear
                    disabled={params.duration !== "date"}
                    placeholder="เลือกวันที่"
                    style={{ width: "100%" }}
                    onChange={(_: any, dateString: string) => {
                      onHandleChangeParams("start", dateString);
                    }}
                  />
                </Col>
                <Col xl={2} style={{ textAlign: "center" }}>
                  <Text>ถึง</Text>
                </Col>
                <Col xl={10}>
                  <DatePicker
                    allowClear
                    disabled={params.duration !== "date"}
                    placeholder="เลือกวันที่"
                    style={{ width: "100%" }}
                    onChange={(_: any, dateString: string) => {
                      onHandleChangeParams("end", dateString);
                    }}
                  />
                </Col>
              </Row>
            </Col>
            <Col lg={10} offset={2}>
              <Row>
                {checkboxes.map((item: ICheckbox) => (
                  <Col xl={8} key={item.value}>
                    <Radio
                      checked={params.duration === item.value}
                      onChange={() =>
                        onHandleChangeParams("duration", item.value)
                      }
                    />
                    <Text style={{ marginLeft: "20px" }}>{item.name}</Text>
                  </Col>
                ))}
              </Row>
            </Col>
            <Col lg={12}>
              <Row gutter={[8, 8]} justify="center" align="middle">
                <Col lg={22} offset={2}>
                  <Input
                    allowClear
                    placeholder="ค้นหาเลขที่ออเดอร์ / ชื่อผู้สั่งซื้อ"
                    suffix={<SearchOutlined />}
                    style={{ width: "100%" }}
                    onChange={(e: any) =>
                      onHandleChangeParams("search", e.target.value)
                    }
                  />
                </Col>
              </Row>
            </Col>
            <Col lg={10} offset={2}>
              <Row gutter={[8, 8]}>
                <Col xl={16}>
                  <Select
                    allowClear
                    placeholder="ค้นหาตามสถานะคำสั่งซื้อ"
                    suffixIcon={<FilterFilled />}
                    style={{ width: "100%" }}
                    options={orderStatus}
                    onChange={(e: any) => {
                      onHandleChangeParams("status", e);
                    }}
                  />
                </Col>
                <Col xl={8}>
                  <ButtonCustom
                    width="100%"
                    type="primary"
                    icon="search"
                    text="ค้นหาออเดอร์"
                    onClick={onFetchData}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </BoxStyled>
        <BoxStyled color="white" type="table">
          <TitleTable title="รายงานคำสั่งซื้อทั้งหมด" />
          <TableMain
            loading={loading}
            scroll={{ x: 1500 }}
            dataSource={dataRows.rows}
            columns={[
              ...ColumnsManageOrder,
              {
                title: "รายละเอียดคำสั่งซื้อ",
                key: "action",
                width: 120,
                align: "center",
                fixed: "right",
                render: (_: any, { code }: any) => (
                  <ButtonCustom
                    type="default"
                    icon="detail"
                    text="รายละเอียด"
                    onClick={() => {
                      navigator(`/admin/product/detail/${code}`);
                    }}
                  />
                ),
              },
            ]}
          />
        </BoxStyled>
      </div>
    </div>
  );
}
