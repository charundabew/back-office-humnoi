import { InfoCircleFilled, LoadingOutlined } from "@ant-design/icons";
import { Col, Row, Typography, message, Upload } from "antd";
import React, { useState } from "react";
import styled from "styled-components";
import { ButtonCustom } from "src/components";
import { RcFile } from "antd/lib/upload";
import { renderImage } from "src/utils/helper";
import { srcPlacholder } from "src/mock/mock-img-src";
import { IFormCoverImage } from "src/interfaces/interface-form";

const { Text } = Typography;

interface IProps {
  onCallBackUpLoadImage: (objImage: IFormCoverImage) => void;
  defaultImage : IFormCoverImage
}

export default function FormUploadOneImage({ onCallBackUpLoadImage ,defaultImage}: IProps) {
  const [uploadImage, setUploadImage] = useState(false);
  const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result as string));
    reader.readAsDataURL(img);
  };

  const beforeUpload = (file: RcFile) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("กรุณาอัปโหลดเฉพาะไฟล์รูปภาพเท่านั้น");
    } else {
      getBase64(file as RcFile, (url) => {
        setUploadImage(true);
        setTimeout(() => {
          const objImage: IFormCoverImage = {
            file: file,
            base64: url,
            imagePath: "",
            product_list_id: undefined,
          };
          onCallBackUpLoadImage(objImage);
          setUploadImage(false);
        }, 500);
      });
    }
  };

  return (
    <div>
      <TextTitle strong>รูปภาพสินค้า</TextTitle>
      <DivTextIcon>
        <InfoCircleFilled style={{ color: "#5c68ff" }} />
        <TextInfo type="secondary">
          อัปโหลดรูปภาพสินค้าจำนวน 1 ภาพเท่านั้น
        </TextInfo>
        <DivUploadCover>
          <Row gutter={[2, 2]} align="middle" justify="center">
            <Col
              xl={11}
              style={{
                textAlign: "center",
                height: "100%",
              }}
            >
              {uploadImage ? (
                <div
                  style={{
                    height: "109px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <LoadingOutlined style={{ fontSize: 36, color: "#5c68ff" }} />
                </div>
              ) : (
                <UploadImage
                  showUploadList={false}
                  beforeUpload={beforeUpload}
                  multiple={false}
                >
                  <ImageCover
                    style={{ cursor: "pointer" }}
                    src={
                      defaultImage.imagePath
                        ? renderImage(defaultImage.imagePath)
                        : defaultImage.base64
                        ? defaultImage.base64
                        : srcPlacholder
                    }
                  ></ImageCover>
                </UploadImage>
              )}
            </Col>
            <Col xl={13} style={{ textAlign: "center" }}>
              <Text type="secondary">อัปโหลดรูปภาพหน้าปกสินค้า </Text>
              <div style={{ marginTop: "15px" }}>
                <Upload
                  showUploadList={false}
                  beforeUpload={beforeUpload}
                  multiple={false}
                >
                  <ButtonCustom
                    icon="upload"
                    type="default"
                    text="เปลี่ยนรูป"
                  />
                </Upload>
              </div>
            </Col>
          </Row>
        </DivUploadCover>
      </DivTextIcon>
    </div>
  );
}

const ImageCover = styled.img`
  height: 105px;
  width: 100%;
  border-radius: 10px;
`;
const TextTitle = styled(Text)`
  font-size: 14px;
`;
const TextInfo = styled(Text)`
  margin-left: 10px;
`;
const DivTextIcon = styled.div`
  color: #90959e;
  margin-top: 5px;
`;
const DivUploadCover = styled.div`
  padding: 16px;
  margin-top: 10px;
  border: 1px dashed #90959e;
  border-radius: 10px;
  background: white;
`;
const UploadImage = styled(Upload)``;
