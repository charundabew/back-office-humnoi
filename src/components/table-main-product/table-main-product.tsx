import { Table, Row, Col, Card } from "antd";
import React from "react";
import { IProductLists } from "src/interfaces/interface-table";
import styled from "styled-components";
import { ButtonCustom } from "src/components";

interface IPropsTable {
  columns: any;
  dataSource: any[];
  scroll?: { x?: number; y?: number };
  loading: boolean;
  pagination: object;
  onDeleteItemList: (id: number) => void;
}

const TableStyled = styled(Table)`
  padding: 0px 10px 0px 10px;
`;

export default function TableMainProduct(props: IPropsTable) {
  const { onDeleteItemList } = props;
  const expandedRowRender = (data: any) => {
    return (
      <Row gutter={[8, 8]} align="middle">
        {data.product_lists.map((item: IProductLists) => (
          <Col key={item.product_id} xl={6}>
            <CardStyled>
              <Row gutter={[8, 4]} align="middle">
                <ColName xl={20}>
                  <div>
                    <TextBrand>ชื่อสีสินค้า</TextBrand>
                    <SpanTitle>{`สี ${item.color}`}</SpanTitle>
                  </div>
                </ColName>
                <Col xl={4}>
                  <ButtonCustom
                    shape="circle"
                    type="dashed"
                    icon="delete"
                    onClick={() => onDeleteItemList(item.id)}
                  />
                </Col>
              </Row>
            </CardStyled>
          </Col>
        ))}
      </Row>
    );
  };
  return (
    <TableStyled
      rowKey="id"
      {...props}
      expandable={{
        columnWidth: 20,
        expandedRowRender,
        rowExpandable: (record: any) => {
          return record.product_lists === undefined ||
            record.product_lists.length === 1
            ? false
            : true;
        },
      }}
      sticky
      size="small"
    />
  );
}

const ColName = styled(Col)`
  display: flex;
  align-items: center;
`;
const SpanTitle = styled.span`
  font-size: 12px;
  font-weight: 500;
  margin-right: 1rem;
  color: #4d556b;
`;
const TextBrand = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  color: grey;
`;
const CardStyled = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 0px 7px 0px 7px;
  background: white;
  & .ant-card-body {
    padding: 10px;
  }
`;
