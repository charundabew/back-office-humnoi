import { Col, Row, Popconfirm, Switch, Tag, Typography, Empty } from "antd";
import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { ICategory } from "src/interfaces/interface-response";
import { USE_HOOK_CATEGORY } from "src/hooks";
import { ButtonCustom } from "src/components";
import { renderImage } from "src/utils/helper";
import { CardWithAction } from "src/styles/card-styled";
import { convertToPrice } from "src/utils/pattern-number";
import _ from "lodash";
import { IProduct } from "src/interfaces/interface-form";
const { Text } = Typography;
export default function FormAddProductSet() {
  const { selectedProduct, onSetToStore } = useContext(GlobalContext);
  const [category, setCategory] = useState<ICategory[]>([]);
  useEffect(() => {
    // declare the data fetching function
    const onPrepareForm = async () => {
      const responseCategory = await USE_HOOK_CATEGORY();
      setCategory(responseCategory);
    };
    // call the function
    onPrepareForm()
      // make sure to catch any error
      .catch(console.error);
  }, []);
  const findCategory = (cateId: number) => {
    const foundItem = category.find((item: ICategory) => item.id === cateId);
    return foundItem?.name ?? "";
  };
  const onDeleteItem = (rowData: IProduct) => {
    const updateData = selectedProduct.filter(
      (item: IProduct) => item.id !== rowData.id
    );
    onSetToStore({ name: "selectedProduct", value: updateData });
  };

  const onHandleChange = (value: any, catId: number) => {
    const updateData = selectedProduct.map((item: IProduct) => {
      item.freebies = item.category_id === catId ? value : item.freebies;
      return item;
    });
    onSetToStore({ name: "selectedProduct", value: updateData });
  };
  return (
    <div>
      <Row gutter={[15, 20]}>
        {selectedProduct.length === 0 ? (
          <Col xs={24}>
            <Empty description="ยังไม่มีรายการสินค้าในเซ็ตนี้" />
          </Col>
        ) : (
          selectedProduct.map((item: IProduct) => (
            <Col xl={8} lg={12} sm={24} xs={24}  key={item.id}>
              <CardWithAction
                actions={[
                  <div>
                    {[0,1, 2, 3].some(
                      (idMap: number) =>
                        item.category_parent_id === idMap
                    ) ? (
                      <Tag color="blue">ถูกเลือกเป็นสินค้าหลัก</Tag>
                    ) : (
                      <div>
                        <Switch
                          checked={item.freebies}
                          onChange={(value: any) =>
                            onHandleChange(value, item.category_id)
                          }
                        />
                        <Text style={{ marginLeft: 10 }}>จัดเป็นของแถม</Text>
                      </div>
                    )}
                  </div>,
                  <Popconfirm
                    title="ต้องการนำสินค้าชิ้นนี้ออกจากเซ็ต?"
                    onConfirm={() => onDeleteItem(item)}
                    okText="ตกลง"
                    cancelText="ยกเลิก"
                  >
                    <ButtonCustom danger type="default" text="นำออกจากเซ็ต" />
                  </Popconfirm>,
                ]}
              >
                <Row gutter={[4, 8]}>
                  <Col xl={18} lg={18} sm={18}>
                    <Tag color="geekblue">{`ประเภทสินค้า : ${findCategory(
                      item.category_id
                    )}`}</Tag>
                    <div style={{ marginTop: 10 }}>
                      <TextBrand>{`ราคา ${convertToPrice(
                        item.price_show ?? item.price
                      )} บาท`}</TextBrand>
                      <SpanTitle>{item.name}</SpanTitle>
                    </div>
                  </Col>
                  <Col xl={6} lg={6} sm={6}>
                    <ImageBrands
                      alt="img-brands"
                      src={renderImage(
                        item.image_path || item.product_lists.image_file || ""
                      )}
                    />
                  </Col>
                </Row>
              </CardWithAction>
            </Col>
          ))
        )}
      </Row>
    </div>
  );
}

const ImageBrands = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 3px;
`;
const SpanTitle = styled.span`
  font-size: 12px;
  font-weight: 500;
  margin-right: 1rem;
`;
const TextBrand = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  color: grey;
`;
