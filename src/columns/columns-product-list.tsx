import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { Avatar, Tag } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { IProductLists, IProductListsImage } from "src/interfaces/interface-table";
import { IMAGE } from "src/services/host";
import { srcPlacholder } from "src/mock/mock-img-src";

const ColumnsProductLists: ColumnsType<IProductLists> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 10,
    // fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "รูปสินค้า",
    dataIndex: '',
    key: "product_list_images",
    align: "center",
    width: 30,
    render: (value : IProductListsImage[]) => (
      <Avatar shape="square" size={48} src={`${IMAGE}${value[0]?.path ?? srcPlacholder}`} />
    ),
  },
  {
    title: "สีสินค้า",
    dataIndex: "color",
    key: "color",
    width: 100,
  },

  {
    title: "สินค้าในคลัง",
    dataIndex: "stock",
    key: "stock",
    align: "center",
    width: 50,
  },

  {
    title: "แก้ไข",
    key: "action",
    width: 20,
    align: "center",
    render: () => (
      <Tag color="blue" icon={<EditOutlined />}>
        แก้ไขสินค้า
      </Tag>
    ),
  },
  {
    title: "ลบ",
    key: "action",
    width: 20,
    align: "center",
    render: () => (
      <Tag color="error" icon={<DeleteOutlined />}>
        ลบสินค้า
      </Tag>
    ),
  },
];

export default ColumnsProductLists;
