import React, { useState, useEffect, useContext } from "react";
import {
  BoxStyled,
  TitleForm,
  FormUploadCoverImage,
  HeaderSubPage,
  FooterActionForm,
  FormUploadOneImage,
} from "src/components";
import {
  Col,
  Form,
  Input,
  notification,
  Radio,
  Row,
  Select,
  Spin,
  Modal,
  Skeleton,
  TreeSelect,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import styled from "styled-components";
import { useNavigate, useParams } from "react-router-dom";
import { USE_HOOK_BRAND, USE_HOOK_SUB_CATEGORY } from "src/hooks";
import { IBrands, ICategories } from "src/interfaces/interface-response";
import {
  defaultCoverImage,
  formNormal,
  formProductLiquid,
  formProductNormal,
} from "src/data/data-initial-value";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { messageAction } from "src/utils/message";
import { AxiosError, AxiosResponse } from "axios";
import { API, AXIOS } from "src/services";
import {
  IFormCoverImage,
  IFormLiquid,
  IFormProductLiquid,
  IOptionsStep1,
} from "src/interfaces/interface-form";
import { RcFile } from "antd/lib/upload";
import { convertToPrice } from "src/utils/pattern-number";
import { propsConfirmAddProduct } from "src/utils/props-component";
import { onKeyDownOnlyNumber } from "src/utils/helper";
const { Option } = Select;
const { confirm } = Modal;
export default function PagenormalProduct() {
  const { parentId, productId, name } = useParams();
  const [defaultImage, setDefaultImage] = useState<IFormCoverImage>({
    product_list_id: NaN,
    imagePath: "",
    base64: "",
    file: {} as RcFile,
  });
  const [form] = Form.useForm();
  const navigator = useNavigate();
  const {
    onSetToStore,
    coverImage,
  }: { onSetToStore: any; coverImage: IFormCoverImage } =
    useContext(GlobalContext);
  const [options, setOptions] = useState<IOptionsStep1>({
    optionsBrand: [],
    optionsCatergory: [],
  });
  const [totalValue, setTotalValue] = useState("");
  const [loading, setLoading] = useState(true);

  const getProductId = () => {
    AXIOS({
      method: "get",
      url: API.PRODUCT_ID,
      params: {
        product_id: productId,
      },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: IFormProductLiquid = data.data;
        const firstItem: IFormLiquid =
          response.product_lists[0] ?? formProductLiquid;
        onSetToStore({
          name: "coverImage",
          value: {
            imagePath: response.image_path,
            file: {} as RcFile,
            base64: "",
            product_list_id: response.id,
          } as IFormCoverImage,
        });
        setDefaultImage({
          imagePath: firstItem.image_file,
          file: {} as RcFile,
          base64: "",
          product_list_id: firstItem.id,
        });
        const updateFormData: IFormLiquid = {
          ...firstItem,
          ...response,
          shipping_free: firstItem.free_shipping,
        };
        form.setFieldsValue({
          ...updateFormData,
        });
        onUpdateTotal();
        setLoading(false);
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });

    // Work with the response...
  };
  const createBranch: any = ({ sub_categories, name, id }: ICategories) => {
    if (sub_categories && sub_categories.length > 0) {
      return {
        title: name,
        value: id,
        children: sub_categories.map((sub: ICategories) => {
          return createBranch(sub);
        }),
      };
    } else {
      return {
        title: name,
        value: id,
        children: [],
      };
    }
  };
  useEffect(() => {
    const onPrepareForm = async () => {
      const responseBrands = await USE_HOOK_BRAND();
      const responseCategory = await USE_HOOK_SUB_CATEGORY(parentId ?? "");
      setOptions({
        optionsBrand: responseBrands,
        optionsCatergory: responseCategory.map((item: ICategories) =>
          createBranch(item)
        ),
      });
      if (productId !== undefined) {
        getProductId();
        onSetToStore({
          name: "coverImage",
          value: {
            ...defaultCoverImage,
          } as IFormCoverImage,
        });
      } else {
        form.setFieldsValue({ ...formProductNormal });
        setLoading(false);
      }
    };
    onPrepareForm();
  }, []);
  const onResetImage = () => {
    onSetToStore({
      name: "coverImage",
      value: {
        imagePath: "",
        file: {} as RcFile,
        base64: "",
        product_list_id: NaN,
      } as IFormCoverImage,
    });
  };
  const onUpdateTotal = () => {
    const values = form.getFieldsValue();
    const { qty, price, discount } = values;
    let result = Number(qty ?? 1) * Number(price ?? 0) - Number(discount ?? 0);
    setTotalValue(convertToPrice(result));
  };
  const onClickCancle = () => {
    form.resetFields();
    onResetImage();
    navigator(-1);
  };
  const validateMessages = {
    required: "ระบุข้อมูล ${label}",
  };

  const onFinish = (values: IFormLiquid) => {
    if (!coverImage.base64 && !coverImage.product_list_id) {
      return notification.warning({
        message: "ระบุข้อมูลสินค้าไม่ครบ",
        description: "กรุณาอัปโหลดรูปภาพสินค้าก่อนบันทึกสินค้า",
      });
    } else {
      confirm({
        ...propsConfirmAddProduct,
        onOk() {
          return new Promise((resolve: any, reject: any) => {
            setLoading(true);
            const { price, stock, discount, qty, coil_size } = values;
            const formData = new FormData();
            const obj = {
              ...values,
              id: productId === undefined ? null : Number(productId),
              price: Number(price),
              discount: Number(discount),
              stock: Number(stock),
              qty: Number(qty),
              coil_size: Number(coil_size),
              product_list_id: defaultImage.product_list_id,
            };
            if (coverImage.base64) {
              formData.append("cover_image", coverImage.file);
            }
            if (defaultImage.base64) {
              formData.append("image", defaultImage.file);
            }
            formData.append("obj", JSON.stringify(obj));

            AXIOS({
              method: productId === undefined ? "post" : "put",
              url: API.PRODUCTS,
              data: formData,
            })
              .then(() => {
                messageAction("saveSuccess");
                onResetImage();
                navigator(-1);
                setLoading(false);
                resolve();
              })
              .catch((error: AxiosError<any, any>) => {
                reject(error);
              });
          }).catch((error) => {
            messageAction("saveFail", error.message);
            setLoading(false);
          });
        },
      });
    }
  };
  const onCallBackUpLoadImage = (objImage: IFormCoverImage) => {
    setDefaultImage(objImage);
  };

  return (
    <div>
      <HeaderSubPage
        header={productId ? `จัดการรายละเอียด` : `เพิ่มสินค้าประเภท${name}`}
        subHeader={
          productId
            ? `จัดการรายละเอียดสินค้า ${name}`
            : `ตรวจสอบข้อมูลสินค้าให้ครบถ้วนก่อนการบันทึกผล`
        }
      />
      <div className="App-paper-page">
        <Spin tip="กำลังโหลดข้อมูล..." spinning={loading}>
          <Form
            layout="vertical"
            scrollToFirstError
            form={form}
            onFinish={onFinish}
            validateMessages={validateMessages}
            initialValues={formNormal}
          >
            <TitleForm type="form" />
            <BoxStyled color="white">
              <Row gutter={[16, 4]} justify="center">
                <Col xl={parentId === "11" ? 17 : 10} sm={4} xs={24}>
                  <Form.Item
                    name="name"
                    label="ชื่อสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input size="large" placeholder="ระบุชื่อสินค้า" />
                  </Form.Item>
                </Col>
                {parentId !== "11" ? (
                  <Col xl={parentId === "7" ? 5 : 7} sm={4} xs={24}>
                    <Form.Item
                      name="brand_id"
                      required
                      label="แบรนด์สินค้า"
                      rules={[{ required: true }]}
                    >
                      <Select
                        showSearch
                        placeholder="เลือกแบรนด์สินค้า"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option!.children as unknown as string)
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                      >
                        {options.optionsBrand.map((brand: IBrands) => (
                          <Option key={brand.id} value={brand.id}>
                            {brand.name}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </Col>
                ) : null}

                <Col xl={parentId === "7" ? 5 : 7} sm={4} xs={24}>
                  <Form.Item
                    name="category_id"
                    required
                    label="ประเภทสินค้า"
                    rules={[{ required: true }]}
                  >
                    <TreeSelect
                      showSearch
                      style={{ width: "100%" }}
                      dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                      placeholder="เลือกประเภทสินค้า"
                      allowClear
                      treeDefaultExpandAll
                      treeData={options.optionsCatergory}
                    ></TreeSelect>
                  </Form.Item>
                </Col>
                {parentId === "7" ? (
                  <Col xl={4} sm={4} xs={24}>
                    <Form.Item
                      label="ขนาดคอยล์ (Coil)"
                      name="coil_size"
                      rules={[{ required: true }]}
                    >
                      <Input
                        onKeyDown={onKeyDownOnlyNumber}
                        type="number"
                        size="large"
                        placeholder="ระบุขนาดคอยล์"
                      />
                    </Form.Item>
                  </Col>
                ) : null}

                <Col xl={5} sm={4} xs={24}>
                  <Form.Item
                    label="จำนวนใน stock"
                    name="stock"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      size="large"
                      addonAfter="ชิ้น"
                      placeholder="0"
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={5} sm={4} xs={24}>
                  <Form.Item
                    name="qty"
                    required
                    label="จำนวนสินค้าต่อหน่วย"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      size="large"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={5} sm={4} xs={24}>
                  <Form.Item
                    name="price"
                    required
                    label="ราคาสินค้า"
                    rules={[{ required: true }]}
                  >
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={5} sm={4} xs={24}>
                  <Form.Item name="discount" label="ส่วนลด">
                    <Input
                      size="large"
                      type="number"
                      addonAfter="บาท"
                      placeholder="00.00"
                      onChange={onUpdateTotal}
                      onKeyDown={onKeyDownOnlyNumber}
                    />
                  </Form.Item>
                </Col>
                <Col xl={4} sm={4} xs={24}>
                  <Form.Item label="ราคารวม">
                    <Input
                      disabled
                      size="large"
                      addonAfter="บาท"
                      placeholder="00.00"
                      value={totalValue}
                    />
                  </Form.Item>
                </Col>

                <Col xl={24}>
                  <Form.Item required label="รายละเอียดสินค้า" name="detail">
                    <TextArea style={{ minHeight: "5rem" }} />
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="shipping_free" required label="การจัดส่ง">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>มีค่าบริการจัดส่ง</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>จัดส่งฟรี</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
                <Col xl={12} xs={24}>
                  <Form.Item name="is_active" required label="สถานะสินค้า ">
                    <Radio.Group style={{ width: "100%" }}>
                      <Row gutter={[16, 8]}>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={true}>
                              <SpanCheckbox>เปิดใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                        <Col xl={12} xs={24}>
                          <DivCheckBox>
                            <Radio value={false}>
                              <SpanCheckbox>ระงับการใช้งาน</SpanCheckbox>
                            </Radio>
                          </DivCheckBox>
                        </Col>
                      </Row>
                    </Radio.Group>
                  </Form.Item>
                </Col>
              </Row>
            </BoxStyled>
            <TitleForm type="images" />
            <BoxStyled color="white">
              <Row gutter={[16, 8]}>
                <Col xl={24} hidden={!loading}>
                  <Skeleton loading={loading} />
                </Col>
                <Col xl={10} sm={12} xs={24} hidden={loading}>
                  <FormUploadCoverImage title="รูปภาพหน้าปก" />
                </Col>
                <Col xl={10} sm={12} xs={24} hidden={loading}>
                  <FormUploadOneImage
                    onCallBackUpLoadImage={onCallBackUpLoadImage}
                    defaultImage={defaultImage}
                  />
                </Col>
              </Row>
            </BoxStyled>
            <FooterActionForm onClickCancle={onClickCancle} />
          </Form>
        </Spin>
      </div>
    </div>
  );
}

const DivCheckBox = styled.div`
  padding: 12px 20px;
  border-radius: 5px;
  background: #f6f8fa;
`;
const SpanCheckbox = styled.span`
  color: #46505e;
`;
