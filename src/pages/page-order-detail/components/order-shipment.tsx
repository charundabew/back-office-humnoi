import { Col, Row, Typography, Tag, Form, Input, DatePicker } from "antd";
import dayjs from "dayjs";
import React, { useEffect } from "react";
import { ButtonCustom } from "src/components";
import { IConfirmShipment, IOrderDetail } from "src/interfaces/interface-order";

import {
  DivRoot,
  Text14,
  TextParagraph,
  TextTitle,
} from "./styled-component-page";
const { Text } = Typography;
interface IProps {
  propsOrder: IOrderDetail;
  onConfirmShipping: (values: IConfirmShipment) => void;
}
export default function OrderShipment({
  propsOrder,
  onConfirmShipping,
}: IProps) {
  const [form] = Form.useForm();
  const {
    status_id,
    shipments,
    addresses,
    status,
    tracking_code,
    shipments_date,
  } = propsOrder;
  const {
    first_name,
    last_name,
    address,
    sub_district,
    district,
    province,
    postcode,
  } = addresses;

  const renderStatusTag = () => {
    if (status_id === 6 || status_id === 5) {
      return <Tag color="#87d068">จัดส่งสำเร็จ</Tag>;
    } else if (status_id === 7) {
      return <Tag>ยังไม่ได้จัดส่ง</Tag>;
    }
  };
  const validateMessages = {
    required: "ระบุข้อมูล ${label}",
  };
  const checkDisabled = () => {
    if (status_id === 3 || status_id === 4) {
      return false;
    } else {
      return true;
    }
  };
  const onSetDefaultForm = () => {
    form.setFieldsValue({
      tracking_number: tracking_code,
      shipping_date: shipments_date ? dayjs(shipments_date) : undefined,
    });
  };

  useEffect(onSetDefaultForm, [shipments_date, shipments_date]);
  return (
    <DivRoot>
      <Form
        disabled={checkDisabled()}
        layout="vertical"
        scrollToFirstError
        form={form}
        onFinish={onConfirmShipping}
        validateMessages={validateMessages}
      >
        <Row style={{ marginTop: "1rem" }}>
          <Col xl={18}>
            <Text14 strong>รายละเอียดที่อยู่การจัดส่ง</Text14>
            <TextTitle style={{ marginTop: "5px" }}>{`ชื่อลูกค้า`}</TextTitle>
            <TextParagraph>{`${first_name} ${last_name}`}</TextParagraph>
            <TextTitle>{`ที่อยู่จัดส่งสินค้า`}</TextTitle>
            <TextParagraph>{`${address} ${sub_district} ${district}`}</TextParagraph>
            <TextParagraph>{`${province} ${postcode}`}</TextParagraph>
          </Col>
          <Col xl={6}>{renderStatusTag()} </Col>
        </Row>
        <Row gutter={[8, 8]}>
          <Col xl={20}>
            <Text14 strong>การจัดส่ง</Text14>
          </Col>
          <Col xl={24}>
            <Text>{`วิธีการจัดส่ง  : ${shipments.name}`}</Text>
          </Col>
          <Col xl={24}>
            <Text>{`วันที่จัดส่งสินค้า  : ${
              shipments_date ? dayjs(shipments_date).format("DD/MM/YYYY") : ""
            }`}</Text>
          </Col>
          <Col xl={24}>
            <Text>{`เลขพัสดุ  : ${tracking_code}`}</Text>
          </Col>
          <Col xl={12}>
            <Form.Item
              hidden={status_id === 5 || status_id === 6}
              label="วันที่จัดส่งสินค้า"
              name="shipping_date"
              rules={[{ required: true }]}
            >
              <DatePicker
                format={"DD/MM/YYYY"}
                placeholder="เลือกวันที่จัดส่งสินค้า"
                style={{ width: "100%" }}
                onChange={(e: any) => {
                  console.log("e", e);
                }}
              />
            </Form.Item>
          </Col>
          <Col xl={12}>
            <Form.Item
              hidden={status_id === 5 || status_id === 6}
              label="เลขพัสดุ"
              name="tracking_number"
              rules={[{ required: true }]}
            >
              <Input style={{ height: 35 }} placeholder="ระบุเลขที่พัสดุ" />
            </Form.Item>
          </Col>
          <Col xl={24} style={{ textAlign: "end" }}>
            <Form.Item hidden={status_id === 5 || status_id === 6}>
              <ButtonCustom
                disabled={checkDisabled()}
                htmlType="submit"
                width="100%"
                type="primary"
                text={checkDisabled() ? status : "ยืนยันการจัดส่งสินค้า"}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </DivRoot>
  );
}
