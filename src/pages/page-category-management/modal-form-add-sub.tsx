import React, { useState, useEffect } from "react";
import { Col, Input, message, Row, Spin, Switch, Typography } from "antd";
import { ButtonCustom } from "src/components";
import { EditFilled } from "@ant-design/icons";
import { ModalFormStyled } from "src/styles/modal-styled";
import {
  ICategories,
  INewSubCategory,
} from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import { AxiosError } from "axios";
import { messageAction } from "src/utils/message";

const { Text } = Typography;

interface IPropsModalCaergory {
  onCloseModal: () => void;
  modalProps: {
    visible: "main" | "sub" | '';
    parent: ICategories;
  };
  onFinishedModal: () => void;
}
export default function ModalFormAddCategory({
  onCloseModal,
  onFinishedModal,
  modalProps: { visible, parent },
}: IPropsModalCaergory) {
  const [formItem, setFormItem] = useState<INewSubCategory>({
    parent_id: 0,
    name: "",
    is_active: false,
  });

  const [loading, setLoading] = useState(false);

  const onSetDefault = () => {
    setFormItem({
      parent_id: parent.id,
      name: "",
      is_active: false,
    });
  };
  useEffect(onSetDefault, [visible]);
  const onHandleChange = (name: string, value: string | boolean) => {
    setFormItem({ ...formItem, [name]: value });
  };

  const onUpdateSub = () => {
    setLoading(true);
    AXIOS({
      method: "post",
      url: API.SUB_CATEGORY,
      data: formItem,
    })
      .then(() => {
        message.success("บักทึกรายการย่อยสำเร็จ!");
        setLoading(false);
        onFinishedModal();
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
        messageAction("saveFail");
      });
  };
  return (
    <div>
      <ModalFormStyled
        width={400}
        title={"เพิ่มประเภทสินค้า"}
        visible={visible === "sub"}
        onCancel={onCloseModal}
        footer={
          <ButtonCustom
            key="submit"
            onClick={onUpdateSub}
            type="primary"
            icon="add"
            text="เพิ่มรายการ"
          />
        }
      >
        <Spin spinning={loading} tip="กำลังบันทึก...">
          <Row gutter={[8, 16]}>
            <Col xl={24}>
              <div style={{ marginBottom: "10px" }}>
                <Text>ชื่อประเภทสินค้าหลัก</Text>
              </div>
              <Input disabled value={parent.name} style={{ width: "100%" }} />
            </Col>
            <Col xl={24}>
              <div style={{ marginBottom: "10px" }}>
                <Text>ชื่อประเภทสินค้าที่ต้องการเพิ่ม</Text>
                <Text style={{ marginLeft: "6px" }} type="secondary">
                  (ความยาวไม่เกิน 50 ตัวอักษร)
                </Text>
              </div>
              <Input
                value={formItem.name}
                suffix={<EditFilled />}
                style={{ width: "100%" }}
                onChange={(e: any) => onHandleChange("name", e.target.value)}
              />
            </Col>
            <Col span={8}>
              <Text>สถานะการเปิดใช้งาน</Text>
            </Col>
            <Col xl={14}>
              <Switch
                checked={formItem.is_active}
                onChange={(e: any) => onHandleChange("is_active", e)}
              />
            </Col>
          </Row>
        </Spin>
      </ModalFormStyled>
    </div>
  );
}
