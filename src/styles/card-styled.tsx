import { Card } from "antd";
import styled from "styled-components";

export const CardTableModalStyled = styled(Card)`
  margin-top: 1rem;
  & .ant-card-body {
    padding: 0px;
  }
  ,
  & .ant-card-head {
    background: #f3f4f6;
    border-radius: 10px;
  }
`;

export const CardWithAction = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 0px 7px 0px 7px;
  background: white;
  & .ant-card-body {
    padding: 12px;
  }
  & .ant-card-actions{
    align-items: center;
    justify-content: center;
  }
`;