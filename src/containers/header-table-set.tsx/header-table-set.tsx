import React from "react";
import { Row, Col, Input, Typography, Select } from "antd";

import { SearchOutlined } from "@ant-design/icons";
import styled from "styled-components";
import IconBoxSet from "src/assets/icons/box-set.svg";
import IconBoxSingle from "src/assets/icons/box-single.svg";
import IconBoxFreebase from "src/assets/icons/icon-freebase.svg";
import IconBoxSaltnic from "src/assets/icons/icon-saltnic.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import { SelectStyled } from "src/styles/select-styled";

const { Text } = Typography;
const { Option } = Select;
interface IPropsHeader {
  cateId: string | undefined;
}
interface IButtonAdd {
  name: string;
  key: string;
  src?: any;
  path: string;
}
export default function HeaderTableSet({ cateId }: IPropsHeader) {
  let navigate = useNavigate();
  const checkIsHiddenRegilar = ["1", "2", "3", "4", "5", "6", "set"];
  const checkIsHiddenSpecial = ["1", "2", "3", "6", "set"];

  const checkHidden = (key: string) => {
    switch (key) {
      case "single":
        return (
          !checkIsHiddenSpecial.some((item: string) => item === cateId) ||
          cateId === "set"
        );
      case "set":
        return !checkIsHiddenSpecial.some((item: string) => item === cateId);
      case "freebase":
        return cateId === "4" ? false : true;
      case "saltnic":
        return cateId === "5" ? false : true;
      case "regular":
        return checkIsHiddenRegilar.some((item: string) => item === cateId);
      default:
        break;
    }
  };
  const buttonsAction: IButtonAdd[] = [
    {
      key: "single",
      name: "สินค้าเดี่ยว",
      src: IconBoxSingle,
      path: `/admin/single-product/${cateId}`,
    },
    {
      key: "set",
      name: "สินค้าแบบเซ็ต",
      src: IconBoxSet,
      path: `/admin/set-product`,
    },
    {
      key: "freebase",
      name: "น้ำยา Freebase",
      src: IconBoxFreebase,
      path: `/admin/freebase-product/${cateId}`,
    },
    {
      key: "saltnic",
      name: "น้ำยา Saltnic",
      src: IconBoxSaltnic,
      path: `/admin/saltnic-product/${cateId}`,
    },
    {
      key: "regular",
      name: "สินค้าแบบปกติ",
      src: IconBoxSaltnic,
      path: `/admin/regular-product/${cateId}`,
    },
  ];
  return (
    <div>
      <BoxRoot>
        <Row gutter={[12, 8]}>
          <Col xl={12} xs={24}>
            <Row gutter={[4, 12]} align="bottom">
              <Col xl={24}>
                <Text strong>{"ค้นหาจากสินค้าทั้งหมด"}</Text>
                <Text style={{ marginLeft: "5px" }} type="secondary">
                  {"(1260 รายการ)"}
                </Text>
              </Col>
              <Col xl={24} xs={24}>
                <BoxActionFilterTableBox>
                  <Row gutter={[4, 4]}>
                    <Col xl={12} xs={24}>
                      <Input
                        placeholder="ค้นหาชื่อสินค้า"
                        suffix={<SearchOutlined />}
                        style={{ width: "100%" }}
                      />
                    </Col>
                    <Col xl={12} xs={24}>
                      <SelectStyled
                        showSearch
                        placeholder="เลือกตามประเภท"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          (option!.children as unknown as string)
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                      >
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        <Option value="tom">Tom</Option>
                      </SelectStyled>
                    </Col>
                  </Row>
                </BoxActionFilterTableBox>
              </Col>
            </Row>
          </Col>
          <Col xl={12} xs={24}>
            <BoxActionButtonTableBox>
              <Row gutter={[8, 12]} justify="end">
                <Col xl={24} style={{ textAlign: "end" }}>
                  <Text style={{ marginLeft: "5px" }} strong>
                    รูปแบบสินค้า
                  </Text>
                  <Text style={{ marginLeft: "5px" }} type="secondary">
                    {"(เลือกเพิ่มสินค้าตามรูปแบบสินค้า)"}
                  </Text>
                </Col>
                {buttonsAction.map((item: IButtonAdd) => (
                  <Col
                    hidden={checkHidden(item.key)}
                    key={item.key}
                    xl={12}
                    xs={12}
                    onClick={() => navigate(item.path)}
                  >
                    <RowStyle align="middle" justify="space-between">
                      <ColIcon xl={6}>
                        <img alt="icon" src={item.src} />
                      </ColIcon>
                      <Col xl={10}>
                        <SpanLabel style={{ fontWeight: 500 }}>
                          {item.name}
                        </SpanLabel>
                      </Col>
                      <Col xl={3}>
                        <FontAwesomeIcon
                          style={{ color: "#4D5FAC" }}
                          fontSize={18}
                          icon={faCirclePlus}
                        />
                      </Col>
                    </RowStyle>
                  </Col>
                ))}
              </Row>
            </BoxActionButtonTableBox>
          </Col>
        </Row>
      </BoxRoot>
    </div>
  );
}
const BoxRoot = styled.div`
  margin: 1.5rem 2rem 2rem 2rem;
`;
const RowStyle = styled(Row)`
  border-radius: 5px;
  background: #f8f9fe;
  height: 4rem;
  cursor: pointer;
  &:hover {
    // border: 1px solid #5c68ff;
    box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  }
`;
const ColIcon = styled(Col)`
  padding: 10px;
  height: 100%;
  display: flex;
  align-items: center;
  border-radius: 5px 0px 0px 5px;
`;
const SpanLabel = styled.span`
  color: #5c68ff;
  fontsize: 14px;
`;
const BoxActionFilterTableBox = styled.div`
  padding: 15px 15px 15px 15px;
  border-radius: 5px;
  background: #f9f9f9;
`;
const BoxActionButtonTableBox = styled.div`
  border-radius: 5px;
  background: #ffff;
`;
