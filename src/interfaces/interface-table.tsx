export interface DataTypeOrder {
  order_no: string;
  order_name: string;
  order_date: Date;
  order_status: string;
  shipping_date: Date;
  track_number: string;
  shipping_price: number;
  product_price: number;
  order_total: number;
  
}

export interface IMockData {
  key: string;
  product_name: string;
  product_cat: string;
  price: number;
  stock: number;
  src: any;
}

export interface IPageProduct {
  name: string;
  single: boolean;
  set: boolean;
  freebase: boolean;
  saltnic: boolean;
  mockData: IMockData;
  dataSourceExpand: IExpandDataCustom[];
}

export interface IExpandDataCustom {
  key: string;
  product_image: string;
  stock: number;
  price: number;
  product_color: string;
}

export interface IProductListsImage {
  id: number;
  path: string;
  product_list_id: number;
  created_at: Date;
  updated_at: Date;
}
export interface IProductLists {
  id: number;
  name: string;
  free_shipping: boolean;
  stock: number;
  sold: number;
  product_id: number;
  detail: string;
  color: string;
  battery: number;
  product_set_id: number;
  product_list_type_id: number;
  product_list_images: IProductListsImage[];
  image_file?: string;
}
export interface IRowsProduct {
  id: number;
  name: string;
  price: number;
  price_show: number;
  qty: number;
  free_shipping: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path: string;
  category_id: number;
  brand_id: number;
  detail: string;
  discount: number;
  product_lists: IProductLists[];
  freebies?: boolean;
  image_file?:string
  category_parent_id: number
  root_parent_id: number
}

export interface IRowOrders {
  id: number;
  code: string;
  status: string;
  status_id: number;
  order_date: Date;
  total_price: number;
  total_discount: number;
  total_net_price: number;
  total_shipping_price: number;
  total_discount_shipping_price: number;
  bank_id: number;
  shipment_id: number;
  address_id: number;
  created_at: Date;
  updated_at: Date;
  customer_name: string;
  tracking_code: string;
  orders_product_lists: [] | null;
}
