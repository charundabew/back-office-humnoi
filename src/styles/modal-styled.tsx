import styled from "styled-components";

import { Modal } from "antd";

export const ModalFormStyled = styled(Modal)`
  & .ant-modal-footer {
    border-radius: 0px 0px 10px 10px;
  }
  & .ant-modal-header {
    background: #373e5c;
    border-radius: 10px 0px 0px 0px;
  }
  & .ant-modal-title {
    color: white;
  }
  & .ant-modal-close-x {
    color: white
  }
`;
