import React from "react";
import { Button } from "antd";
import {
  PlusOutlined,
  FileExcelFilled,
  FileTextOutlined,
  SaveFilled,
  CloudUploadOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartPlus,
  faArrowCircleLeft,
  faTimesCircle,
  faFileEdit,
  faTrashCan,
  faGear,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
interface IPropsHeader {
  danger?: boolean;
  htmlType?: "button" | "submit" | "reset";
  text?: string;
  width?: string | number;
  size?: "small" | "middle" | "large";
  type?:
    | "primary"
    | "link"
    | "text"
    | "default"
    | "ghost"
    | "dashed"
    | undefined;
  shape?: "default" | "circle" | "round";
  icon?:
    | "add"
    | "save"
    | "excel"
    | "detail"
    | "delete"
    | "edit"
    | "upload"
    | "back"
    | "cancel"
    | "cart-add"
    | "edit-dashed"
    | "search";
  onClick?: () => void;
  disabled?: false | true;
}

const ButtonStyled = styled(Button)`
  height: 32px;
  font-size: 12px;
  &.ant-btn-default {
    border-radius: 5px;
    border: 1px solid #5c68ff;
    color: #5151f9;
    :disabled {
      border: 1px solid #d9d9d9;
      color: rgba(0, 0, 0, 0.25);
      background: #f5f5f5;
    }
  }
  &.ant-btn-round.ant-btn-sm {
    border-radius: 10px;
  }
  &.ant-btn-primary {
    border-radius: 5px;
    font-size: 12px;
  }
  &.ant-btn-ghost {
    border-radius: 5px;
    color: white;
    border: unset;
    background-color: #8e8eaf;
  }
  &.ant-btn-text {
    color: #5151f9;
    font-weight: 500;
  }
  &.ant-btn-dashed {
    color: #5151f9;
    font-weight: 500;
    border: 1px dashed #5c68ff;
  }
  &.ant-btn-dangerous {
    border-color: #ff4d4e;
    color: #ff4d4e;
  }
`;
export default function ButtonCustom({
  text,
  type,
  icon,
  disabled,
  onClick,
  shape,
  width,
  size,
  danger,
  htmlType,
}: IPropsHeader) {
  const renderIcon = (icon: any) => {
    switch (icon) {
      case "add":
        return <PlusOutlined />;
      case "excel":
        return <FileExcelFilled />;
      case "detail":
        return <FileTextOutlined />;
      case "delete":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 12 }}
            icon={faTrashCan}
          ></FontAwesomeIcon>
        );
      case "edit-dashed":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 12 }}
            icon={faGear}
          ></FontAwesomeIcon>
        );
      case "save":
        return <SaveFilled />;
      case "edit":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 14, marginRight: 6 }}
            icon={faFileEdit}
          ></FontAwesomeIcon>
        );
      case "upload":
        return <CloudUploadOutlined />;
      case "back":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 14, marginRight: 6 }}
            icon={faArrowCircleLeft}
          />
        );
      case "cart-add":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 14, marginRight: 6 }}
            icon={faCartPlus}
          />
        );
      case "cancel":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 14, marginRight: 10 }}
            icon={faTimesCircle}
          />
        );
      case "search":
        return (
          <FontAwesomeIcon
            style={{ fontSize: 14, marginRight: 10 }}
            icon={faMagnifyingGlass}
          />
        );
      default:
        break;
    }
  };
  return (
    <div>
      <ButtonStyled
        danger={danger}
        htmlType={htmlType}
        size={size}
        shape={shape}
        disabled={disabled}
        onClick={onClick}
        icon={renderIcon(icon)}
        type={type}
        style={{ width: width }}
      >
        {text}
      </ButtonStyled>
    </div>
  );
}
