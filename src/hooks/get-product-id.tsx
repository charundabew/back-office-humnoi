import { AxiosResponse } from "axios";
import { formProductSingle } from "src/data/data-initial-value";
import {
  IFormProductSingle,
} from "src/interfaces/interface-form";
import { AXIOS, API } from "src/services";

const USE_HOOK_PRODUCT_ID = async (id: string | undefined) => {
  try {
    let { data }: AxiosResponse<any, any> = await AXIOS({
      method: "get",
      url: API.PRODUCT_ID,
      params: {
        product_id: id,
      },
    });
    return data.data as IFormProductSingle ;
    // Work with the response...
  } catch (err: any) {
    // Handle error
    return formProductSingle;
  }
};

export default USE_HOOK_PRODUCT_ID;
