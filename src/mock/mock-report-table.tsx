
export const MockReportTable = {
    product_image: 'https://www.ecigelm.com/wp-content/uploads/2022/06/Sprite-Salt-Nic.jpg.webp',
    product_name: "Sprite Salt Nic",
    product_type: 'น้ำยา Saltnic',
    total_amount: 25,
    price: 300,
    total: 2560,
    cost: 120,
    total_cost: 4560,
    profit: 34,
    total_profit: 2323
}