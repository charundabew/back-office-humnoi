//fix ไว้หน้าบ้าน

import { IPermissionUser } from "src/interfaces/interface-form";

const userPermission: IPermissionUser[] = [
  {
    id: 1,
    type_name: "owner",
    access_path: "/owner",
  },
  {
    id: 2,
    type_name: "admin",
    access_path: "/admin",
  },
];

export default userPermission;
